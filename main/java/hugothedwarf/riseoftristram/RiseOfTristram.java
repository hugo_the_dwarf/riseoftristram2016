package hugothedwarf.riseoftristram;

import hugothedwarf.riseoftristram.comms.ServerProxy;
import hugothedwarf.riseoftristram.managers.ManagerCapability;
import hugothedwarf.riseoftristram.managers.ManagerEvent;
import hugothedwarf.riseoftristram.managers.ManagerPacket;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = RiseOfTristram.MODID, version = RiseOfTristram.VERSION, name = RiseOfTristram.MODNAME)
public class RiseOfTristram
{
	public static final String MODID = "riseoftristram";
	public static final String MODNAME = "Rise of Tristram";
	public static final String VERSION = "1.10.2.0.0";

	@Instance(value = MODID)
	public static RiseOfTristram INSTANCE;

	@SidedProxy(clientSide = "hugothedwarf.riseoftristram.comms.ClientProxy", serverSide = "hugothedwarf.riseoftristram.comms.ServerProxy")
	public static ServerProxy proxy;

	// Packet network
	public static ManagerPacket packetManager;

	@EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
		proxy.preInit(event);
		ManagerPacket.registerPackets();
		ManagerCapability.registerCapabilities();
		ManagerEvent.registerEvents();
	}

	@EventHandler
	public void init(FMLInitializationEvent event)
	{
		proxy.init(event);
		proxy.registerGuiHandler();
	}

	@EventHandler
	public void postInit(FMLPostInitializationEvent event)
	{
		proxy.postInit(event);
	}

	// FurnaceRecipes.instance().getSmeltingList() or CraftingManager.getInstance().getRecipeList()

	/* Getting the recipe result while looping through the crafting list is very easy, as the IRecipe interface includes getRecipeOutput(). So that'll let you narrow in on the right recipe quickly.
	 * From there, you just need to convert it to the proper recipe class (there are several) and get the inputs. Do note that (most!) tools are of type ShapedOreRecipe and they're made up of lists of
	 * items in each slot, rather than a single item (I had to write a recipe comparison function last night that would take in an arbitrary recipe and find recipe with the same pattern, but using a
	 * different material: i.e. given the recipe for a wooden axe, find the recipe for the golden axe; that was fun). */
}
