package hugothedwarf.riseoftristram.libs;

public class ModValues
{
	public static final int ATTRIBUTE_MAX_LIMIT = 2000;
	public static final int LEVEL_MAX = 500;
	public static final int RECURSIVE_LOOP_MAX = 50;
	public static final float BASE_MAX_MANA_STAM = 100f;

	public static final int OTHER_DIMENSION_ATTRIBUTE_BONUS = 125;
	public static final int OTHER_DIMENSION_LEVEL_BONUS = 75;

	public static final int BOSS_ATTRIBUTE_BONUS = 95;
	public static final int BOSS_LEVEL_BONUS = 45;

	public static final int BASE_MIN_DAMAGE = 1;
	public static final int BASE_MAX_DAMAGE = 10;
	public static final int BASE_UPSCALE = 5;
	public static final int MIN_DAMAGE = 1, MAX_DAMAGE = 10;
	public static final float DEF_MAX_PERCENT_REDUCTION = 0.6f;
	public static final float DEF_RATE_OVERLOAD = 250f;
	public static final float DEF_REDUCTION_RATE = (DEF_RATE_OVERLOAD * 10) / (DEF_MAX_PERCENT_REDUCTION * 10);
	public static final float DEF_FLAT_REDUCTION_OVERLOAD = 0.05f;

	public static final int HP_REGEN_TIME = 30;
	public static final int MP_REGEN_TIME = 22;
	public static final int SP_REGEN_TIME = 16;

	public static final float SPRINT_STAM_COST = 0.1f;
}
