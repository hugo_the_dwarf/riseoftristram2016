package hugothedwarf.riseoftristram.libs;

public class PlayerProfession
{
	// Unlike Classes, you just need to know what professions you are
	// As the title is what determines the passive effects and skills
	public String professionName;
	public String professionDesc;
	public int[] skillIds;

	public PlayerProfession(String professionName, String professionDesc, int[] skillIds)
	{
		this.professionName = professionName;
		this.professionDesc = professionDesc;
		this.skillIds = skillIds;
	}

}
