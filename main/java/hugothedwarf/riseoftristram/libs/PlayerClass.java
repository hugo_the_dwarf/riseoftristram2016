package hugothedwarf.riseoftristram.libs;

import hugothedwarf.riseoftristram.capability.rpgValues.CapRPGData;
import hugothedwarf.riseoftristram.libs.componets.Stat;
import hugothedwarf.riseoftristram.managers.ManagerRPGStat;

public class PlayerClass
{
	private float dmgPerStr = 0f, dmgPerInt = 0f, dmgPerDex = 0f, maxDmgPercent = 0.1f;
	private float hpPerVit = 1f, manaPerIntStat = 1f, stamPerVitStat = 1f;
	private String className;
	public int[] skillIds;
	public Stat[] stats;

	public PlayerClass(String className, int[] skillIds, Stat[] stats)
	{
		this.className = className;
		this.skillIds = skillIds;
		this.stats = stats;
	}

	public String getName()
	{
		return className;
	}

	public PlayerClass setDmgPerStr(float value)
	{
		this.dmgPerStr = value;
		return this;
	}

	public PlayerClass setDmgPerInt(float value)
	{
		this.dmgPerInt = value;
		return this;
	}

	public PlayerClass setDmgPerDex(float value)
	{
		this.dmgPerDex = value;
		return this;
	}

	public PlayerClass setMaxDmgPercent(float value)
	{
		this.maxDmgPercent = value;
		return this;
	}

	public float getMinDmg(CapRPGData cap)
	{
		return (1);
	}

	public float getMaxDmg(CapRPGData capAttrbiute)
	{
		return (getMinDmg(capAttrbiute) * (1 + maxDmgPercent));
	}

	public PlayerClass setHPPerVit(float value)
	{
		this.hpPerVit = value;
		return this;
	}

	public float getHpPerVit()
	{
		return hpPerVit;
	}

	public PlayerClass setManaPerIntStat(float value)
	{
		this.manaPerIntStat = value;
		return this;
	}

	public float getManaPerIntStat()
	{
		return manaPerIntStat;
	}

	public PlayerClass setStamPerVitStat(float value)
	{
		this.stamPerVitStat = value;
		return this;
	}

	public float getStamPerVitStat()
	{
		return stamPerVitStat;
	}
}
