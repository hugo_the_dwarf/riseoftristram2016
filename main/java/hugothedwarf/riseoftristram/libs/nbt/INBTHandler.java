package hugothedwarf.riseoftristram.libs.nbt;

import net.minecraft.item.ItemStack;

public interface INBTHandler
{
	void setTag(ItemStack stack);
}
