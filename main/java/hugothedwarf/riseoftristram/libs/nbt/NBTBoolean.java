package hugothedwarf.riseoftristram.libs.nbt;

import hugothedwarf.riseoftristram.libs.UtilNBTHelper;
import net.minecraft.item.ItemStack;

public class NBTBoolean implements INBTHandler
{
	public boolean keyValue = false;
	public String keyName = null;

	public NBTBoolean(String keyName, boolean keyValue)
	{
		this.keyName = keyName;
		this.keyValue = keyValue;
	}

	@Override
	public void setTag(ItemStack stack)
	{
		if (keyName != null) UtilNBTHelper.setBoolean(stack, keyName, keyValue);
	}

}
