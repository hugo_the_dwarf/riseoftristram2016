package hugothedwarf.riseoftristram.libs.componets;

public class ItemQuality
{
	private float modifier;
	private String qualityName;

	public ItemQuality(int modifier, String qualityName)
	{
		this.modifier = modifier;
		this.qualityName = qualityName;
	}

	public float getModifier()
	{
		return modifier;
	}

	public String getName()
	{
		return qualityName;
	}

}
