package hugothedwarf.riseoftristram.libs.componets;

import hugothedwarf.riseoftristram.capability.rpgValues.CapRPG;
import hugothedwarf.riseoftristram.capability.rpgValues.CapRPGData;
import hugothedwarf.riseoftristram.libs.ModValues;
import hugothedwarf.riseoftristram.libs.nbt.INBTHandler;
import net.minecraft.item.ItemStack;

public class ItemAffix
{
	private String name = "";
	private String namePrefix = "";
	public int rankLowRequirement = 1;
	public int rankHighRequirement = ModValues.LEVEL_MAX;
	private Stat[] stats = null;
	private INBTHandler[] nbts = null;
	private ItemAffixEnchantment[] enchantments = null;
	private boolean[] allowedTypes = new boolean[]
	{
		false,
		false,
		false,
		false,
		false
	};
	public static final int melee = 0, bow = 1, armor = 2, shield = 3, trinket = 4;

	public String getName(boolean full)
	{
		if (full && namePrefix != "") return namePrefix + " " + name;
		return name;
	}

	public ItemAffix setMelee()
	{
		allowedTypes[melee] = true;
		return this;
	}

	public boolean isForMelee()
	{
		return allowedTypes[melee];
	}

	public ItemAffix setBow()
	{
		allowedTypes[bow] = true;
		return this;
	}

	public boolean isForBow()
	{
		return allowedTypes[bow];
	}

	public ItemAffix setArmor()
	{
		allowedTypes[armor] = true;
		return this;
	}

	public boolean isForArmor()
	{
		return allowedTypes[armor];
	}

	public ItemAffix setShield()
	{
		allowedTypes[shield] = true;
		return this;
	}

	public boolean isForShield()
	{
		return allowedTypes[shield];
	}

	public ItemAffix setTrinket()
	{
		allowedTypes[trinket] = true;
		return this;
	}

	public boolean isForTrinket()
	{
		return allowedTypes[trinket];
	}

	public ItemAffix setStats(Stat[] stats)
	{
		this.stats = stats;
		return this;
	}

	public ItemAffix setStat(Stat stat)
	{
		if (stat != null) this.stats = new Stat[]
		{
			stat
		};
		return this;
	}

	public ItemAffix setNBTs(INBTHandler[] nbts)
	{
		this.nbts = nbts;
		return this;
	}

	public ItemAffix setNBT(INBTHandler nbt)
	{
		if (nbt != null) this.nbts = new INBTHandler[]
		{
			nbt
		};
		return this;
	}

	public ItemAffix setEnchantments(ItemAffixEnchantment[] enchantments)
	{
		this.enchantments = enchantments;
		return this;
	}

	public ItemAffix setEnchantment(ItemAffixEnchantment enchantment)
	{
		if (enchantment != null) this.enchantments = new ItemAffixEnchantment[]
		{
			enchantment
		};
		return this;
	}

	public void applyValuesToItemStack(ItemStack stack)
	{
		CapRPGData capAttribute = stack.getCapability(CapRPG.CAPABILITY, null);

		if (this.stats != null)
		{
			for (Stat s : this.stats)
			{
				capAttribute.baseStats[s.getId()].setValue(capAttribute.baseStats[s.getId()].getValue() + s.getValue());
			}
		}

		if (this.nbts != null)
		{
			for (INBTHandler n : this.nbts)
			{
				n.setTag(stack);
			}
		}

		if (this.enchantments != null)
		{
			for (ItemAffixEnchantment e : this.enchantments)
			{
				stack.addEnchantment(e.getEnchantment(), e.getLevel());
			}
		}
	}

	public ItemAffix(String name, String namePrefix, int lowRank, int highRank)
	{
		this.name = name;
		this.namePrefix = namePrefix;
		this.rankLowRequirement = lowRank;
		this.rankHighRequirement = highRank;
	}

	public ItemAffix(String name, int lowRank, int highRank)
	{
		this.name = name;
		this.rankLowRequirement = lowRank;
		this.rankHighRequirement = highRank;
	}
}
