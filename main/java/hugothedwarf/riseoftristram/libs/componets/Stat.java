package hugothedwarf.riseoftristram.libs.componets;

import hugothedwarf.riseoftristram.RiseOfTristram;
import hugothedwarf.riseoftristram.libs.HTDUtil;

public class Stat
{
	private String statName = "";
	private String NBTTagName = "";
	private int id = -1;
	private float value = 0f;
	private float randomMax = 0f;

	public Stat(String name, String tag, int id)
	{
		this.statName = name;
		this.NBTTagName = tag;
		this.id = id;
	}
	
	public Stat(int id)
	{
		this.id = id;
	}

	public Stat setValue(float value)
	{
		this.value = value;
		return this;
	}

	public Stat setValue(float value, float randomMax)
	{
		this.randomMax = randomMax;
		setValue(value);
		return this;
	}
	
	public int getId()
	{
		return this.id;
	}

	public int getValueInt()
	{
		if (this.randomMax > 0) return (int) (this.value + (float) (HTDUtil.rand.nextInt((int) randomMax)));
		return (int) this.value;
	}

	public float getValue()
	{
		if (this.randomMax > 0) return (this.value + (float) (HTDUtil.rand.nextInt((int) randomMax)));
		return this.value;
	}

	public float getValuePercent()
	{
		if (this.randomMax > 0) return (this.value + ((float) (HTDUtil.rand.nextInt((int) (randomMax * 100))) / 100));
		return this.value / 100f;
	}

	public String getNBTTag()
	{
		return RiseOfTristram.MODID.toUpperCase() + this.NBTTagName;
	}

	public String getTag()
	{
		return this.NBTTagName;
	}

	public String getName()
	{
		return this.statName;
	}
}
