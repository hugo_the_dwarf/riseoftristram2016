package hugothedwarf.riseoftristram.libs.componets;

import net.minecraft.item.ItemStack;

public class SkillCost
{
	private float healthCost = 0f;
	private float percentCurrentHealthCost = 0f;
	private float percentMaxHealthCost = 0f;

	private float manaCost = 0f;
	private float percentCurrentManaCost = 0f;
	private float percentMaxManaCost = 0f;

	private float stamCost = 0f;
	private float percentCurrentStamCost = 0f;
	private float percentMaxStamCost = 0f;

	private ItemStack[] items;

	public SkillCost()
	{

	}

	public SkillCost setHealthCost(float value)
	{
		this.healthCost = value;
		return this;
	}

	public SkillCost setPercentCurrentHealthCost(float value)
	{
		this.percentCurrentHealthCost = value;
		return this;
	}

	public SkillCost setPercentMaxHealthCost(float value)
	{
		this.percentMaxHealthCost = value;
		return this;
	}

	public SkillCost setManaCost(float value)
	{
		this.manaCost = value;
		return this;
	}

	public SkillCost setPercentCurrentManaCost(float value)
	{
		this.percentCurrentManaCost = value;
		return this;
	}

	public SkillCost setPercentMaxManaCost(float value)
	{
		this.percentMaxManaCost = value;
		return this;
	}

	public SkillCost setStamCost(float value)
	{
		this.stamCost = value;
		return this;
	}

	public SkillCost setPercentCurrentStamCost(float value)
	{
		this.percentCurrentStamCost = value;
		return this;
	}

	public SkillCost setPercentMaxStamCost(float value)
	{
		this.percentMaxStamCost = value;
		return this;
	}

	public SkillCost setItemCost(ItemStack[] items)
	{
		this.items = items;
		return this;
	}

	public SkillCost setItemCost(ItemStack item)
	{
		this.items = new ItemStack[]{item};
		return this;
	}

}
