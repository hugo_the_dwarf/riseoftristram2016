package hugothedwarf.riseoftristram.libs.componets;

import java.util.Random;

import hugothedwarf.riseoftristram.libs.ModValues;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class Loot
{
	private Item item = null;
	private Block block = null;
	private int cost = 1;
	private int lowestRange = 1;
	private int higestRange = ModValues.LEVEL_MAX;
	private int baseQuantity = 1;
	private int maxQuantity = 1;
	private int meta = 0;
	private boolean randomMeta = false;
	private Random rand = new Random();

	public Loot(Item item, int lowestRange, int highestRange, int cost, int baseQuantity, int maxQuantity)
	{
		this.item = item;
		this.lowestRange = lowestRange;
		this.higestRange = highestRange;
		this.cost = cost;
		this.baseQuantity = baseQuantity;
		this.maxQuantity = maxQuantity;
	}

	public Loot(Item item, int lowestRange, int highestRange, int cost, int baseQuantity)
	{
		this.item = item;
		this.lowestRange = lowestRange;
		this.higestRange = highestRange;
		this.cost = cost;
		this.baseQuantity = baseQuantity;
	}

	public Loot(Item item, int lowestRange, int highestRange, int cost)
	{
		this.item = item;
		this.lowestRange = lowestRange;
		this.higestRange = highestRange;
		this.cost = cost;
	}

	public Loot(Block block, int lowestRange, int highestRange, int cost, int baseQuantity, int maxQuantity)
	{
		this.block = block;
		this.lowestRange = lowestRange;
		this.higestRange = highestRange;
		this.cost = cost;
		this.baseQuantity = baseQuantity;
		this.maxQuantity = maxQuantity;
	}

	public Loot(Block block, int lowestRange, int highestRange, int cost, int baseQuantity)
	{
		this.block = block;
		this.lowestRange = lowestRange;
		this.higestRange = highestRange;
		this.cost = cost;
		this.baseQuantity = baseQuantity;
	}

	public Loot(Block block, int lowestRange, int highestRange, int cost)
	{
		this.block = block;
		this.lowestRange = lowestRange;
		this.higestRange = highestRange;
		this.cost = cost;
	}

	public boolean canLoot(int points, int level)
	{
		if (points >= this.cost) if (level >= lowestRange && (level <= higestRange || higestRange == ModValues.LEVEL_MAX)) return true;
		return false;
	}
	
	public boolean canLoot(int points)
	{		
		return canLoot(points,points);
	}

	public int getCost()
	{
		return this.cost;
	}

	public ItemStack get()
	{
		int newQuantity = baseQuantity;
		int newMeta = 0;

		if (maxQuantity > baseQuantity)
		{
			if (baseQuantity > 1) newQuantity += rand.nextInt((maxQuantity - (baseQuantity - 1)));
			else newQuantity += rand.nextInt(maxQuantity);
		}

		if (randomMeta) newMeta = rand.nextInt(meta);
		else newMeta = meta;

		if (block != null) return new ItemStack(block, newQuantity, newMeta);
		return new ItemStack(item, newQuantity, newMeta);
	}

	public Loot setMeta(int meta)
	{
		this.meta = meta;
		return this;
	}

	public Loot setRandomMeta(int metaRange)
	{
		this.meta = metaRange;
		this.randomMeta = true;
		return this;
	}
}
