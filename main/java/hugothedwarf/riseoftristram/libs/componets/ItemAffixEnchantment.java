package hugothedwarf.riseoftristram.libs.componets;

import net.minecraft.enchantment.Enchantment;

public class ItemAffixEnchantment
{
	private Enchantment enchant = null;
	private int level = 0;

	public ItemAffixEnchantment(Enchantment enchant, int level)
	{
		this.enchant = enchant;
		this.level = level;
	}
	
	public Enchantment getEnchantment()
	{
		return Enchantment.getEnchantmentByID(Enchantment.getEnchantmentID(enchant));
	}
	
	public int getLevel()
	{
		return level;
	}
}
