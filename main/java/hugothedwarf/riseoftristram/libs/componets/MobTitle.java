package hugothedwarf.riseoftristram.libs.componets;

public class MobTitle
{
	private int baseBonus;
	private String titleName;
	private float dmgPerStat = 0.5f;
	private float dmgMaxPercent = 1.2f;
	private float hpPerVit = 1f;
	private float defPerVit = 0.25f;

	public MobTitle(String titleName, int baseBonus)
	{
		this.titleName = titleName;
		this.baseBonus = baseBonus;
	}

	public int getBaseBonus()
	{
		return this.baseBonus;
	}

	public String getTitleName()
	{
		return this.titleName;
	}

	public MobTitle setHealthPerVit(float value)
	{
		this.hpPerVit = value;
		return this;
	}

	public float getHealthPerVit()
	{
		return this.hpPerVit;
	}

	public MobTitle setDefencePerVit(float value)
	{
		this.defPerVit = value;
		return this;
	}

	public float getDefencePerVit()
	{
		return this.defPerVit;
	}

	public MobTitle setDamagePerStat(float value)
	{
		this.dmgPerStat = value;
		return this;
	}

	public float getDamagePerStat()
	{
		return this.dmgPerStat;
	}

	public MobTitle setMaxDamagePercent(float value)
	{
		this.dmgMaxPercent = value;
		return this;
	}

	public float getMaxDamagePercent()
	{
		return this.dmgMaxPercent;
	}

}
