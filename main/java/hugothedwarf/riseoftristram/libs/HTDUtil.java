package hugothedwarf.riseoftristram.libs;

import java.util.List;
import java.util.Random;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.entity.item.EntityItemFrame;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemBow;
import net.minecraft.item.ItemShield;
import net.minecraft.item.ItemSword;
import net.minecraft.item.ItemTool;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;

public class HTDUtil
{

	public static Random rand = new Random();

	public static boolean isItemWeapon(Item item)
	{
		if (item instanceof ItemSword || item instanceof ItemTool || item instanceof ItemBow) return true;
		return false;
	}

	public static boolean isItemEquipment(Item item)
	{
		if (isItemWeapon(item) || item instanceof ItemShield || item instanceof ItemArmor) return true;
		return false;
	}

	public static boolean entityCanAttack(Entity entity)
	{
		// return entity.isCreatureType(EnumCreatureType.MONSTER, false);
		if (entity instanceof EntityLiving)
		{
			EntityLiving ent = (EntityLiving) entity;
			if (!ent.targetTasks.taskEntries.isEmpty()) return true;
		}
		return false;
	}

	public static int advRandom(float left, float offsetAdjust, int power, float powerOffset)
	{
		int successes = 0;
		float leftOffset = left;
		float OffsetPercent = offsetAdjust * ((float) Math.pow(powerOffset, power));
		float currentRoll = 0;
		for (int i = 0; i < ModValues.RECURSIVE_LOOP_MAX; i++)
		{
			currentRoll = rand.nextFloat();
			if (currentRoll >= leftOffset)
			{
				successes++;
				leftOffset *= (1 + OffsetPercent);
			}
			else break;
		}

		return successes;
	}

	public static int advRandom(float left, float offsetAdjust)
	{
		return advRandom(left, offsetAdjust, 0, 0f);
	}

	// Get whatever the player is looking at
	/** Entity is the creature you want to use to "look" from Range is how many blocks out it will find something **/
	public Entity getEntitesFromLine(Entity entity, double range)
	{
		// Entity entity = this.mc.getRenderViewEntity();
		Minecraft mc = Minecraft.getMinecraft();

		if (entity != null)
		{
			if (mc.theWorld != null)
			{
				mc.mcProfiler.startSection("pick");
				mc.pointedEntity = null;
				mc.objectMouseOver = entity.rayTrace(range, 0f);// partial tick
																// time?
				double d1 = range;
				Vec3d vec3 = entity.getPositionEyes(0f);// partial tick time?

				if (mc.objectMouseOver != null)
				{
					d1 = mc.objectMouseOver.hitVec.distanceTo(vec3);
				}

				Vec3d vec31 = entity.getLook(0f);// partial tick time?
				Vec3d vec32 = vec3.addVector(vec31.xCoord * range, vec31.yCoord * range, vec31.zCoord * range);
				Entity pointedEntity = null;
				Vec3d vec33 = null;
				float f1 = 1.0F;
				List list = mc.theWorld.getEntitiesWithinAABBExcludingEntity(entity,
						entity.getEntityBoundingBox().addCoord(vec31.xCoord * range, vec31.yCoord * range, vec31.zCoord * range).expand((double) f1, (double) f1, (double) f1));
				double d2 = d1;

				for (int i = 0; i < list.size(); ++i)
				{
					Entity entity1 = (Entity) list.get(i);

					if (entity1.canBeCollidedWith())
					{
						float f2 = entity1.getCollisionBorderSize();
						AxisAlignedBB axisalignedbb = entity1.getEntityBoundingBox().expand((double) f2, (double) f2, (double) f2);
						RayTraceResult movingobjectposition = axisalignedbb.calculateIntercept(vec3, vec32);

						if (axisalignedbb.isVecInside(vec3))
						{
							if (0.0D < d2 || d2 == 0.0D)
							{
								pointedEntity = entity1;
								vec33 = movingobjectposition == null ? vec3 : movingobjectposition.hitVec;
								d2 = 0.0D;
							}
						}
						else if (movingobjectposition != null)
						{
							double d3 = vec3.distanceTo(movingobjectposition.hitVec);

							if (d3 < d2 || d2 == 0.0D)
							{
								if (entity1 == entity.getRidingEntity() && !entity.canRiderInteract())
								{
									if (d2 == 0.0D)
									{
										pointedEntity = entity1;
										vec33 = movingobjectposition.hitVec;
									}
								}
								else
								{
									pointedEntity = entity1;
									vec33 = movingobjectposition.hitVec;
									d2 = d3;
								}
							}
						}
					}
				}

				if (pointedEntity != null && (d2 < d1 || mc.objectMouseOver == null))
				{
					mc.objectMouseOver = new RayTraceResult(pointedEntity, vec33);

					if (pointedEntity instanceof EntityLivingBase || pointedEntity instanceof EntityItemFrame)
					{
						mc.pointedEntity = pointedEntity;
					}
				}
				mc.mcProfiler.endSection();
				return pointedEntity;
			}
		}
		return null;
	}

	/** Sets throwable heading based on an entity that's throwing it */
	public Vec3d setHeadingFromEntity(Entity entityThrower, float rotationPitchIn, float rotationYawIn, float pitchOffset, float velocity, float inaccuracy)
	{
		double velX, velY, velZ;

		double f = -MathHelper.sin(rotationYawIn * 0.017453292F) * MathHelper.cos(rotationPitchIn * 0.017453292F);
		double f1 = -MathHelper.sin((rotationPitchIn + pitchOffset) * 0.017453292F);
		double f2 = MathHelper.cos(rotationYawIn * 0.017453292F) * MathHelper.cos(rotationPitchIn * 0.017453292F);

		double a = MathHelper.sqrt_double(f * f + f1 * f1 + f2 * f2);
		f = f / a;
		f1 = f1 / a;
		f2 = f2 / a;
		f = f + rand.nextGaussian() * 0.007499999832361937D * (double) inaccuracy;
		f1 = f1 + rand.nextGaussian() * 0.007499999832361937D * (double) inaccuracy;
		f2 = f2 + rand.nextGaussian() * 0.007499999832361937D * (double) inaccuracy;
		f = f * (double) velocity;
		f1 = f1 * (double) velocity;
		f2 = f2 * (double) velocity;
		velX = f;
		velY = f1;
		velZ = f2;
		float a1 = MathHelper.sqrt_double(f * f + f2 * f2);

		velX += entityThrower.motionX;
		velZ += entityThrower.motionZ;

		if (!entityThrower.onGround)
		{
			velY += entityThrower.motionY;
		}
		return new Vec3d(velX, velY, velZ);
	}
}
