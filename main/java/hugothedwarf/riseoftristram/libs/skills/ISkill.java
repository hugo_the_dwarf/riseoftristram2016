package hugothedwarf.riseoftristram.libs.skills;

import hugothedwarf.riseoftristram.libs.componets.SkillCost;
import net.minecraft.entity.Entity;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public interface ISkill
{
	Runnable trigger(int targetEntityId, int ownerEntityId, int selectedSkillSlot, MessageContext ctx);

	void update();
	void activate(int targetEntityId, int ownerEntityId, World world);
	void activate(int targetEntityId, Entity ownerEntity, World world);
	int getSkillRange();
	boolean canUse();	
	SkillCost getCost();
}
