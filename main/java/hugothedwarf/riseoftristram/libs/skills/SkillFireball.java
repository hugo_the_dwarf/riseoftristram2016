package hugothedwarf.riseoftristram.libs.skills;

import hugothedwarf.riseoftristram.RiseOfTristram;
import hugothedwarf.riseoftristram.capability.playerextra.CapPlayerExtra;
import hugothedwarf.riseoftristram.capability.playerextra.CapPlayerExtraData;
import hugothedwarf.riseoftristram.capability.rpgValues.CapRPG;
import hugothedwarf.riseoftristram.capability.rpgValues.CapRPGData;
import hugothedwarf.riseoftristram.comms.packets.SkillActivatePacket;
import hugothedwarf.riseoftristram.libs.HTDUtil;
import hugothedwarf.riseoftristram.managers.ManagerPacket;
import hugothedwarf.riseoftristram.managers.ManagerSkill;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.projectile.EntitySmallFireball;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;

public class SkillFireball extends SkillBase implements ISkill
{

	private static SkillData skillData = new SkillData(2, 30, 0);
	private static int myID = ManagerSkill.SKILL_FIREBALL;

	@Override
	public int getSkillRange()
	{
		return 0;
	}

	@Override
	public Runnable triggerPlayer(int targetEntityId, int skillSlot, MessageContext ctx)
	{
		if (ctx.side == Side.CLIENT)
		{
			return new Runnable() {
				public void run()
				{
					EntityPlayer player = RiseOfTristram.proxy.getClientPlayer();
					CapPlayerExtraData capPlayerExtra = player.getCapability(CapPlayerExtra.CAPABILITY, null);
					ManagerSkill.setSkillCooldown(capPlayerExtra, skillSlot, skillData);
					capPlayerExtra.playerInfo[skillSlot] = skillData.coolDown;
				}
			};
		}
		else if (ctx.side == Side.SERVER)
		{
			return new Runnable() {
				public void run()
				{
					EntityPlayerMP player = ctx.getServerHandler().playerEntity;
					CapRPGData capAttributes = player.getCapability(CapRPG.CAPABILITY, null);
					if (canUseSkill(skillData, capAttributes))
					{
						player.worldObj.playEvent((EntityPlayer) null, 1018,
								new BlockPos((int) player.posX, (int) player.posY, (int) player.posZ), 0);
						Vec3d vec = new HTDUtil().setHeadingFromEntity(player, player.rotationPitch, player.rotationYaw,
								0f, 20f, 0f);
						EntitySmallFireball entitysmallfireball = new EntitySmallFireball(player.worldObj, player,
								vec.xCoord, vec.yCoord, vec.zCoord);
						entitysmallfireball.posY = player.posY + (double) (player.height / 2.0F) + 0.5D;
						player.worldObj.spawnEntityInWorld(entitysmallfireball);

						// Send a Response packet?

						useSkill(skillData, capAttributes);
						CapPlayerExtraData capPlayerExtra = player.getCapability(CapPlayerExtra.CAPABILITY, null);
						ManagerSkill.setSkillCooldown(capPlayerExtra, skillSlot, skillData);
						ManagerPacket.INSTANCE.sendTo(new SkillActivatePacket(myID, -1, skillSlot), player);
					}
				}
			};
		}
		return null;
	}

	@Override
	public Runnable triggerMob(int targetEntityId, int skillSlot)
	{
		// TODO Auto-generated method stub
		return null;
	}

}
