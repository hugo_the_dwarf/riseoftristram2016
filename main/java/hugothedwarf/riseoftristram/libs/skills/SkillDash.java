package hugothedwarf.riseoftristram.libs.skills;

import hugothedwarf.riseoftristram.RiseOfTristram;
import hugothedwarf.riseoftristram.capability.rpgValues.CapRPG;
import hugothedwarf.riseoftristram.capability.rpgValues.CapRPGData;
import hugothedwarf.riseoftristram.comms.packets.SkillPacket;
import hugothedwarf.riseoftristram.libs.componets.SkillCost;
import hugothedwarf.riseoftristram.managers.ManagerPacket;
import hugothedwarf.riseoftristram.managers.ManagerSkill;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;

public class SkillDash implements ISkill
{

	public static int myID = ManagerSkill.SKILL_DASH;
	private float staminaCost = 55f;
	public final int useCooldown = 7;
	public int currentCooldown = 0;
	private int aiTargetRange = 10;

	@Override
	public int getSkillRange()
	{
		return aiTargetRange;
	}

	@Override
	public Runnable trigger(int targetEntityId, int ownerEntityId, int selectedSkillSlot, MessageContext ctx)
	{
		if (canUse())
		{
			if (ctx.side == Side.CLIENT)
			{
				return new Runnable() {
					public void run()
					{
						EntityPlayer player = RiseOfTristram.proxy.getClientPlayer();
						activate(targetEntityId, player.getEntityId(),player.worldObj);
					}
				};
			}
			else if (ctx.side == Side.SERVER)
			{
				return new Runnable() {
					public void run()
					{
						EntityPlayerMP player = ctx.getServerHandler().playerEntity;
						CapRPGData cap = player.getCapability(CapRPG.CAPABILITY, null);
						if (1 == 1)
						{
							ManagerPacket.INSTANCE.sendTo(new SkillPacket(myID, -1, selectedSkillSlot), player);
						}
					}
				};
			}
		}
		return null;
	}

	@Override
	public void update()
	{
		if (currentCooldown > 0) currentCooldown--;
	}

	private void use()
	{
		currentCooldown = useCooldown;
	}

	@Override
	public boolean canUse()
	{
		return currentCooldown <= 0;
	}

	@Override
	public void activate(int targetEntityId, int ownerEntityId, World world)
	{
		Entity ownerEnt = null;
		if (ownerEntityId != -1) ownerEnt = world.getEntityByID(ownerEntityId);
		logic(null,ownerEnt);
	}
	
	private void logic(Entity targetEnt, Entity ownerEnt)
	{
		ownerEnt.motionX += ownerEnt.getLookVec().xCoord * 1.5;
		ownerEnt.motionY += ownerEnt.getLookVec().yCoord;
		ownerEnt.motionZ += ownerEnt.getLookVec().zCoord * 1.5;
		use();
	}

	@Override
	public SkillCost getCost()
	{
		return new SkillCost().setStamCost(staminaCost);
	}

	@Override
	public void activate(int targetEntityId, Entity ownerEntity, World world)
	{
		logic(null,ownerEntity);		
	}

}
