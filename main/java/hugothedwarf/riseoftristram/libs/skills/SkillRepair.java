package hugothedwarf.riseoftristram.libs.skills;

import hugothedwarf.riseoftristram.RiseOfTristram;
import hugothedwarf.riseoftristram.capability.playerextra.CapPlayerExtra;
import hugothedwarf.riseoftristram.capability.playerextra.CapPlayerExtraData;
import hugothedwarf.riseoftristram.capability.rpgValues.CapRPG;
import hugothedwarf.riseoftristram.capability.rpgValues.CapRPGData;
import hugothedwarf.riseoftristram.comms.packets.SkillActivatePacket;
import hugothedwarf.riseoftristram.managers.ManagerPacket;
import hugothedwarf.riseoftristram.managers.ManagerSkill;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;

public class SkillRepair extends SkillBase implements ISkill
{

	private static SkillData skillData = new SkillData(5, 0, 0);
	private static final float MANA_PER_DAMAGE = 0.15f;
	private static final float STAM_PER_DAMAGE = 0.425f;
	private static final int MAX_DUR_INTERVAL_COST = 1500;
	private static final int MAX_COST_MULT = 10;
	private static int myID = ManagerSkill.SKILL_REPAIR;

	@Override
	public int getSkillRange()
	{
		return 0;
	}

	@Override
	public Runnable triggerPlayer(int targetEntityId, int skillSlot, MessageContext ctx)
	{
		if (ctx.side == Side.CLIENT)
		{
			return new Runnable() {
				public void run()
				{
					EntityPlayer player = RiseOfTristram.proxy.getClientPlayer();
					CapPlayerExtraData capPlayerExtra = player.getCapability(CapPlayerExtra.CAPABILITY, null);
					ManagerSkill.setSkillCooldown(capPlayerExtra, skillSlot, skillData);
				}
			};
		}
		else if (ctx.side == Side.SERVER)
		{
			return new Runnable() {
				public void run()
				{
					EntityPlayerMP player = ctx.getServerHandler().playerEntity;
					CapRPGData capAttributes = player.getCapability(CapRPG.CAPABILITY, null);
					InventoryPlayer inv = player.inventory;
					ItemStack is;
					ItemStack[] items = new ItemStack[6];
					int numberOfItems = 0;
					boolean hasFixed = false;
					boolean repairLimit = false;

					float manaCost = 0;
					float stamCost = 0;
					float finalManaCost = 0;
					float finalStamCost = 0;

					if (inv.mainInventory[inv.currentItem] != null)
					{
						items[numberOfItems] = inv.mainInventory[inv.currentItem];
						numberOfItems++;
					}

					if (inv.offHandInventory[0] != null)
					{
						items[numberOfItems] = inv.offHandInventory[0];
						numberOfItems++;
					}

					for (int i = 0; i < 4; i++)
					{
						if (inv.armorInventory[i] != null)
						{
							items[numberOfItems] = inv.armorInventory[i];
							numberOfItems++;
						}
					}

					for (int i = 0; i < numberOfItems; i++)
					{
						is = items[i];
						if (is != null && is.isItemDamaged())
						{
							int itemMaxDur = is.getMaxDamage();
							int costMultipier = 1;
							while (itemMaxDur >= MAX_DUR_INTERVAL_COST)
							{
								itemMaxDur -= MAX_DUR_INTERVAL_COST;
								costMultipier++;
							}
							manaCost = costMultipier * MANA_PER_DAMAGE;
							stamCost = costMultipier * STAM_PER_DAMAGE;

							int repairPoints = 0;

							while (capAttributes.canConsumeManaStam(finalManaCost + (manaCost * (repairPoints + 1)),
									finalStamCost + (stamCost * (repairPoints + 1)))
									&& repairPoints < is.getItemDamage())
							{
								repairPoints++;
							}

							if (repairPoints > 0)
							{
								finalManaCost += manaCost * repairPoints;
								finalStamCost += stamCost * repairPoints;
								is.setItemDamage(
										is.getItemDamage() >= repairPoints ? is.getItemDamage() - repairPoints : 0);
								capAttributes.consumeManaStam(finalManaCost, finalStamCost);
								hasFixed = true;
							}
						}
					}

					// Send a Response packet?
					if (hasFixed)
					{
						useSkill(skillData, capAttributes);
						CapPlayerExtraData capPlayerExtra = player.getCapability(CapPlayerExtra.CAPABILITY, null);
						ManagerSkill.setSkillCooldown(capPlayerExtra, skillSlot, skillData);
						ManagerPacket.INSTANCE.sendTo(new SkillActivatePacket(myID, -1, skillSlot), player);
					}
				}
			};
		}
		return null;
	}

	@Override
	public Runnable triggerMob(int targetEntityId, int skillSlot)
	{
		// TODO Auto-generated method stub
		return null;
	}

}
