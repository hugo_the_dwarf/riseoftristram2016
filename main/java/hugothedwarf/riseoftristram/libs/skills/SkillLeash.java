package hugothedwarf.riseoftristram.libs.skills;

import hugothedwarf.riseoftristram.RiseOfTristram;
import hugothedwarf.riseoftristram.capability.playerextra.CapPlayerExtra;
import hugothedwarf.riseoftristram.capability.playerextra.CapPlayerExtraData;
import hugothedwarf.riseoftristram.capability.rpgValues.CapRPG;
import hugothedwarf.riseoftristram.capability.rpgValues.CapRPGData;
import hugothedwarf.riseoftristram.comms.packets.SkillActivatePacket;
import hugothedwarf.riseoftristram.managers.ManagerPacket;
import hugothedwarf.riseoftristram.managers.ManagerSkill;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;

public class SkillLeash extends SkillBase implements ISkill
{

	private static SkillData skillData = new SkillData(1, 0, 0);
	private static int myID = ManagerSkill.SKILL_LEASH;
	private int range = 7;

	@Override
	public int getSkillRange()
	{
		return range;
	}

	@Override
	public Runnable triggerPlayer(int targetEntityId, int skillSlot, MessageContext ctx)
	{
		if (ctx.side == Side.CLIENT)
		{
			return new Runnable() {
				public void run()
				{
					EntityPlayer player = RiseOfTristram.proxy.getClientPlayer();
					CapPlayerExtraData capPlayerExtra = player.getCapability(CapPlayerExtra.CAPABILITY, null);
					ManagerSkill.setSkillCooldown(capPlayerExtra, skillSlot, skillData);
				}
			};
		}
		else if (ctx.side == Side.SERVER)
		{
			return new Runnable() {
				public void run()
				{
					EntityPlayerMP player = ctx.getServerHandler().playerEntity;
					Entity entity = ctx.getServerHandler().playerEntity.worldObj.getEntityByID(targetEntityId);
					CapRPGData capAttributes = player.getCapability(CapRPG.CAPABILITY, null);
					if (entity instanceof EntityLiving)
					{
						EntityLiving ent = (EntityLiving) entity;
						if (!ent.getLeashed()) ent.setLeashedToEntity(player, true);
						else ent.clearLeashed(true, false);

						// Send a Response packet?

						CapPlayerExtraData capPlayerExtra = player.getCapability(CapPlayerExtra.CAPABILITY, null);
						ManagerSkill.setSkillCooldown(capPlayerExtra, skillSlot, skillData);
						ManagerPacket.INSTANCE.sendTo(new SkillActivatePacket(myID, -1, skillSlot), player);

					}
				}
			};
		}
		return null;
	}

	@Override
	public Runnable triggerMob(int targetEntityId, int skillSlot)
	{
		// TODO Auto-generated method stub
		return null;
	}

}
