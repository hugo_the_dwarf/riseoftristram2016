package hugothedwarf.riseoftristram.libs.skills;

import hugothedwarf.riseoftristram.RiseOfTristram;
import hugothedwarf.riseoftristram.capability.playerextra.CapPlayerExtra;
import hugothedwarf.riseoftristram.capability.playerextra.CapPlayerExtraData;
import hugothedwarf.riseoftristram.capability.rpgValues.CapRPG;
import hugothedwarf.riseoftristram.capability.rpgValues.CapRPGData;
import hugothedwarf.riseoftristram.comms.packets.SkillActivatePacket;
import hugothedwarf.riseoftristram.managers.ManagerPacket;
import hugothedwarf.riseoftristram.managers.ManagerSkill;
import net.minecraft.entity.Entity;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.monster.EntityZombie;
import net.minecraft.entity.passive.EntityVillager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.MobEffects;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;

public class SkillZombieCure extends SkillBase implements ISkill
{

	private static SkillData skillData = new SkillData(20, 150, 0);
	private static int myID = ManagerSkill.SKILL_ZOMBIE_CURE;
	private int range = 10;

	@Override
	public int getSkillRange()
	{
		return range;
	}

	@Override
	public Runnable triggerPlayer(int targetEntityId, int skillSlot, MessageContext ctx)
	{
		if (ctx.side == Side.CLIENT)
		{
			return new Runnable() {
				public void run()
				{
					EntityPlayer player = RiseOfTristram.proxy.getClientPlayer();
					CapPlayerExtraData capPlayerExtra = player.getCapability(CapPlayerExtra.CAPABILITY, null);
					ManagerSkill.setSkillCooldown(capPlayerExtra, skillSlot, skillData);
				}
			};
		}
		else if (ctx.side == Side.SERVER)
		{
			return new Runnable() {
				public void run()
				{
					EntityPlayerMP player = ctx.getServerHandler().playerEntity;
					Entity entity = ctx.getServerHandler().playerEntity.worldObj.getEntityByID(targetEntityId);
					CapRPGData capAttributes = player.getCapability(CapRPG.CAPABILITY, null);
					if (entity instanceof EntityZombie && ((EntityZombie) entity).isVillager()
							&& canUseSkill(skillData, capAttributes))
					{
						useSkill(skillData, capAttributes);
						EntityZombie zombie = (EntityZombie) entity;

						EntityVillager entityvillager = new EntityVillager(zombie.worldObj);
						entityvillager.copyLocationAndAnglesFrom(zombie);
						entityvillager.onInitialSpawn(
								zombie.worldObj.getDifficultyForLocation(new BlockPos(entityvillager)),
								(IEntityLivingData) null);
						entityvillager.setLookingForHome();

						if (zombie.isChild())
						{
							entityvillager.setGrowingAge(-24000);
						}

						zombie.worldObj.removeEntity(zombie);
						entityvillager.setNoAI(zombie.isAIDisabled());
						if (zombie.getVillagerTypeForge() != null)
							entityvillager.setProfession(zombie.getVillagerTypeForge());
						else entityvillager.setProfession(0);

						if (zombie.hasCustomName())
						{
							entityvillager.setCustomNameTag(zombie.getCustomNameTag());
							entityvillager.setAlwaysRenderNameTag(zombie.getAlwaysRenderNameTag());
						}

						zombie.worldObj.spawnEntityInWorld(entityvillager);
						entityvillager.addPotionEffect(new PotionEffect(MobEffects.NAUSEA, 200, 0));
						zombie.worldObj.playEvent((EntityPlayer) null, 1027,
								new BlockPos((int) zombie.posX, (int) zombie.posY, (int) zombie.posZ), 0);

						// Send a Response packet?

						CapPlayerExtraData capPlayerExtra = player.getCapability(CapPlayerExtra.CAPABILITY, null);
						ManagerSkill.setSkillCooldown(capPlayerExtra, skillSlot, skillData);
						ManagerPacket.INSTANCE.sendTo(new SkillActivatePacket(myID, -1, skillSlot), player);

					}
				}
			};
		}
		return null;
	}

	@Override
	public Runnable triggerMob(int targetEntityId, int skillSlot)
	{
		// TODO Auto-generated method stub
		return null;
	}

}
