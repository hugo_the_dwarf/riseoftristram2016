package hugothedwarf.riseoftristram.comms.packets;

import hugothedwarf.riseoftristram.managers.ManagerSkill;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;

public class SkillPacket implements IMessage
{

	public static class SkillHandler implements IMessageHandler<SkillPacket, IMessage>
	{
		@Override
		public IMessage onMessage(SkillPacket message, MessageContext ctx)
		{
			if (message.selectedSkillSlot != -1)
			{
				//Stat Capability object
				if (ctx.side == Side.CLIENT)
				{
					//Get Client World
					//Look Up Entity by ownerEntityId
					//set Capability Object
					//get skill by using selectedSkillId
					//Modify below to pass in all 4 variables into the skills trigger method
					Minecraft.getMinecraft().addScheduledTask(ManagerSkill.getSkillLogic(message.targetEntityId, message.ownerEntityId, message.selectedSkillSlot, ctx));
				}
				else if (ctx.side == Side.SERVER)
				{
					//Get server world
					//Look up Entity
					ctx.getServerHandler().playerEntity.getServerWorld().addScheduledTask(ManagerSkill.t(message.targetEntityId, message.ownerEntityId, message.selectedSkillSlot, ctx));
				}
			}

			return null;
		}

	}

	int targetEntityId = -1;
	int ownerEntityId = -1;
	int selectedSkillSlot = -1;

	public SkillPacket(int targetEntityId, int ownerEntityId, int selectedSkillSlot)
	{
		this.targetEntityId = targetEntityId;
		this.ownerEntityId = ownerEntityId;
		this.selectedSkillSlot = selectedSkillSlot;
	}

	@Override
	public void fromBytes(ByteBuf buf)
	{
		targetEntityId = buf.readInt();
		ownerEntityId = buf.readInt();
		selectedSkillSlot = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		buf.writeInt(targetEntityId);
		buf.writeInt(ownerEntityId);
		buf.writeInt(selectedSkillSlot);
	}

}
