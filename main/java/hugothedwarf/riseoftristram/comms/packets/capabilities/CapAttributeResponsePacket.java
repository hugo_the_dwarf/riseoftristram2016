package hugothedwarf.riseoftristram.comms.packets.capabilities;

import hugothedwarf.riseoftristram.RiseOfTristram;
import hugothedwarf.riseoftristram.capability.rpgValues.CapRPG;
import hugothedwarf.riseoftristram.managers.ManagerRPGStat;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class CapAttributeResponsePacket implements IMessage
{

	public static class CapAttributePacketHandler implements IMessageHandler<CapAttributeResponsePacket, IMessage>
	{

		@Override
		public IMessage onMessage(CapAttributeResponsePacket message, MessageContext ctx)
		{
			EntityPlayer player = RiseOfTristram.proxy.getClientPlayer();
			Minecraft.getMinecraft().addScheduledTask(new Runnable() {
				public void run()
				{
					if (player != null)
					{
						player.getCapability(CapRPG.CAPABILITY, null).setStatValues(message.data);
					}
				}
			});
			return null;
		}

	}

	int dataSize = ManagerRPGStat.STAT_NAMES.length;

	float[] data = new float[dataSize];

	public CapAttributeResponsePacket()
	{}

	public CapAttributeResponsePacket(float[] data)
	{
		this.data = data;
	}

	@Override
	public void fromBytes(ByteBuf buf)
	{
		for (int index = 0; index < dataSize; index++)
		{
			data[index] = buf.readFloat();
		}
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		for (int index = 0; index < dataSize; index++)
		{
			buf.writeFloat((float) data[index]);
		}
	}

}
