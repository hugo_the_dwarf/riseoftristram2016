package hugothedwarf.riseoftristram.comms.packets;

import hugothedwarf.riseoftristram.RiseOfTristram;
import hugothedwarf.riseoftristram.client.gui.GuiHandler;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ClassGUIPacket implements IMessage
{

	public static class ClassGUIPacketHandler implements IMessageHandler<ClassGUIPacket, IMessage>
	{

		@Override
		public IMessage onMessage(ClassGUIPacket message, MessageContext ctx)
		{
			// Open the gui on the server so we can actually do stuff.
			EntityPlayer player = ctx.getServerHandler().playerEntity;
			ctx.getServerHandler().playerEntity.getServerWorld().addScheduledTask(new Runnable() {
				public void run()
				{
					player.openGui(RiseOfTristram.INSTANCE, GuiHandler.CLASS_GUI, player.worldObj, (int) player.posX, (int) player.posY, (int) player.posZ);
				}
			});
			return null;
		}
	}

	public ClassGUIPacket()
	{}

	public ClassGUIPacket(String text)
	{}

	@Override
	public void fromBytes(ByteBuf buf)
	{}

	@Override
	public void toBytes(ByteBuf buf)
	{}

}
