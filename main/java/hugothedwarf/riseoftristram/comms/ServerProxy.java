package hugothedwarf.riseoftristram.comms;

import hugothedwarf.riseoftristram.RiseOfTristram;
import hugothedwarf.riseoftristram.client.gui.GuiHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;

public class ServerProxy
{

	public void preInit(FMLPreInitializationEvent event)
	{}

	public void init(FMLInitializationEvent event)
	{}

	public void postInit(FMLPostInitializationEvent event)
	{}

	public void registerGuiHandler()
	{
		NetworkRegistry.INSTANCE.registerGuiHandler(RiseOfTristram.INSTANCE, new GuiHandler());
	}

	public EntityPlayer getClientPlayer()
	{
		return null;
	}
}
