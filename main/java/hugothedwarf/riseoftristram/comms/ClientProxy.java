package hugothedwarf.riseoftristram.comms;

import hugothedwarf.riseoftristram.events.client.EventGui;
import hugothedwarf.riseoftristram.events.client.EventPlayerOverlayGui;
import hugothedwarf.riseoftristram.events.client.KeyHandleEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

public class ClientProxy extends ServerProxy
{
	@Override
	public void preInit(FMLPreInitializationEvent event)
	{}

	@Override
	public void init(FMLInitializationEvent event)
	{
		MinecraftForge.EVENT_BUS.register(new KeyHandleEvent());
		KeyHandleEvent.registerKeys();
		MinecraftForge.EVENT_BUS.register(new EventPlayerOverlayGui(Minecraft.getMinecraft()));
		MinecraftForge.EVENT_BUS.register(new EventGui());
	}

	@Override
	public void postInit(FMLPostInitializationEvent event)
	{}

	@Override
	public EntityPlayer getClientPlayer()
	{
		return Minecraft.getMinecraft().thePlayer;
	}
}
