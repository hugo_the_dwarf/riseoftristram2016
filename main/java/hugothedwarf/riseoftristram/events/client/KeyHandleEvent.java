package hugothedwarf.riseoftristram.events.client;

import org.lwjgl.input.Keyboard;

import hugothedwarf.riseoftristram.capability.playerextra.CapPlayerExtra;
import hugothedwarf.riseoftristram.capability.playerextra.CapPlayerExtraData;
import hugothedwarf.riseoftristram.client.gui.GuiPlayerStats;
import hugothedwarf.riseoftristram.comms.packets.SkillActivatePacket;
import hugothedwarf.riseoftristram.libs.HTDUtil;
import hugothedwarf.riseoftristram.managers.ManagerPacket;
import hugothedwarf.riseoftristram.managers.ManagerSkill;
import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;

public class KeyHandleEvent
{
	static String category = "keys.riseoftristram";
	public static KeyBinding classKey = new KeyBinding("Class Menu", Keyboard.KEY_Y, category);
	public static KeyBinding skill1 = new KeyBinding("Use Skill 1", Keyboard.KEY_R, category);
	public static KeyBinding skill2 = new KeyBinding("Use Skill 2", Keyboard.KEY_C, category);

	public static String getKeyBindingName(KeyBinding key)
	{
		return Keyboard.getKeyName(key.getKeyCode());
	}

	@SubscribeEvent
	public void keyHandler(InputEvent.KeyInputEvent e)
	{
		EntityPlayer player = Minecraft.getMinecraft().thePlayer;
		CapPlayerExtraData capPlayerExtra = player.getCapability(CapPlayerExtra.CAPABILITY, null);
		Entity ent = null;
		HTDUtil utilHelper = new HTDUtil();

		if (classKey.isPressed())
		{
			// PacketManager.INSTANCE.sendToServer(new ClassGUIPacket());
			Minecraft.getMinecraft().displayGuiScreen(new GuiPlayerStats(player));
		}
		if (skill1.isPressed())
		{
			if (player.isSneaking())
			{
				if (capPlayerExtra.playerInfo[capPlayerExtra.SKILL_3] != -1 && capPlayerExtra.playerInfo[capPlayerExtra.SKILL_3_CD] == 0)
				{
					ent = utilHelper.getEntitesFromLine(player, ManagerSkill.skills[capPlayerExtra.playerInfo[capPlayerExtra.SKILL_3]].getSkillRange());
					int entityId = ent != null ? ent.getEntityId() : -1;
					Minecraft.getMinecraft().addScheduledTask(new Runnable() {
						public void run()
						{
							ManagerPacket.INSTANCE.sendToServer(new SkillActivatePacket(capPlayerExtra.playerInfo[capPlayerExtra.SKILL_3], entityId, capPlayerExtra.SKILL_3_CD));
						}
					});
				}
			}
			else
			{
				if (capPlayerExtra.playerInfo[capPlayerExtra.SKILL_1] != -1 && capPlayerExtra.playerInfo[capPlayerExtra.SKILL_1_CD] == 0)
				{
					ent = utilHelper.getEntitesFromLine(player, ManagerSkill.skills[capPlayerExtra.playerInfo[capPlayerExtra.SKILL_1]].getSkillRange());
					int entityId = ent != null ? ent.getEntityId() : -1;
					Minecraft.getMinecraft().addScheduledTask(new Runnable() {
						public void run()
						{
							ManagerPacket.INSTANCE.sendToServer(new SkillActivatePacket(capPlayerExtra.playerInfo[capPlayerExtra.SKILL_1], entityId, capPlayerExtra.SKILL_1_CD));
						}
					});
				}
			}
		}

		if (skill2.isPressed())
		{
			if (player.isSneaking())
			{
				if (capPlayerExtra.playerInfo[capPlayerExtra.SKILL_4] != -1 && capPlayerExtra.playerInfo[capPlayerExtra.SKILL_4_CD] == 0)
				{
					ent = utilHelper.getEntitesFromLine(player, ManagerSkill.skills[capPlayerExtra.playerInfo[capPlayerExtra.SKILL_4]].getSkillRange());
					int entityId = ent != null ? ent.getEntityId() : -1;
					Minecraft.getMinecraft().addScheduledTask(new Runnable() {
						public void run()
						{
							ManagerPacket.INSTANCE.sendToServer(new SkillActivatePacket(capPlayerExtra.playerInfo[capPlayerExtra.SKILL_4], entityId, capPlayerExtra.SKILL_4_CD));
						}
					});
				}
			}
			else
			{
				if (capPlayerExtra.playerInfo[capPlayerExtra.SKILL_2] != -1 && capPlayerExtra.playerInfo[capPlayerExtra.SKILL_2_CD] == 0)
				{
					ent = utilHelper.getEntitesFromLine(player, ManagerSkill.skills[capPlayerExtra.playerInfo[capPlayerExtra.SKILL_2]].getSkillRange());
					int entityId = ent != null ? ent.getEntityId() : -1;
					Minecraft.getMinecraft().addScheduledTask(new Runnable() {
						public void run()
						{
							ManagerPacket.INSTANCE.sendToServer(new SkillActivatePacket(capPlayerExtra.playerInfo[capPlayerExtra.SKILL_2], entityId, capPlayerExtra.SKILL_2_CD));
						}
					});
				}
			}
		}

		if (Minecraft.getMinecraft().thePlayer.getName() == "ToxicShad0w" || Minecraft.getMinecraft().thePlayer.getName() == "Hugo_the_Dwarf")
		{
			if (new KeyBinding("", Keyboard.KEY_C, "").isPressed())
			{
				// This is where the super secret give-player-OP-gear code goes.
				// TODO write the super secret give-player-OP-gear code.
				// For testing I'm gonna open a gui
				// Rot.net.sendToServer(new ClassProfessionInfoGUIPacket());
			}
		}
	}

	public static void registerKeys()
	{
		ClientRegistry.registerKeyBinding(classKey);
		ClientRegistry.registerKeyBinding(skill1);
		ClientRegistry.registerKeyBinding(skill2);
	}

}
