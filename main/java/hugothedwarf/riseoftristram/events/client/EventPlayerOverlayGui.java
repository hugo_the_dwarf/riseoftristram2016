package hugothedwarf.riseoftristram.events.client;

import java.awt.Color;

import org.lwjgl.opengl.GL11;

import hugothedwarf.riseoftristram.RiseOfTristram;
import hugothedwarf.riseoftristram.capability.mobextra.CapMobExtra;
import hugothedwarf.riseoftristram.capability.mobextra.CapMobExtraData;
import hugothedwarf.riseoftristram.capability.playerextra.CapPlayerExtra;
import hugothedwarf.riseoftristram.capability.playerextra.CapPlayerExtraData;
import hugothedwarf.riseoftristram.capability.rpgValues.CapRPG;
import hugothedwarf.riseoftristram.capability.rpgValues.CapRPGData;
import hugothedwarf.riseoftristram.comms.packets.capabilities.CapMobUpdatePacket;
import hugothedwarf.riseoftristram.libs.HTDUtil;
import hugothedwarf.riseoftristram.managers.ManagerRPGStat;
import hugothedwarf.riseoftristram.managers.ManagerPacket;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class EventPlayerOverlayGui extends Gui
{
	// Registered in ClientProxy
	private Minecraft mc;
	private static final ResourceLocation hms_base = new ResourceLocation(RiseOfTristram.MODID + ":textures/gui/hms_base.png");
	private static final ResourceLocation manaTexture = new ResourceLocation(RiseOfTristram.MODID + ":textures/gui/mana_bar.png");
	private static final ResourceLocation staminaTexture = new ResourceLocation(
			RiseOfTristram.MODID + ":textures/gui/stam_bar.png");
	private static final ResourceLocation healthTexture = new ResourceLocation(RiseOfTristram.MODID + ":textures/gui/hp_bar.png");
	private static final ResourceLocation skillTexture = new ResourceLocation(RiseOfTristram.MODID + ":textures/gui/skills.png");
	private static final ResourceLocation targetTexture = new ResourceLocation(
			RiseOfTristram.MODID + ":textures/gui/viewed_health.png");

	private int barW = 48, barH = 12;
	private int baseW = barW, baseH = 8;
	private int skillHW = 22;

	public EventPlayerOverlayGui(Minecraft mc)
	{
		super();
		this.mc = mc;
	}

	@SubscribeEvent(priority = EventPriority.NORMAL)
	public void onRenderExperienceBar(RenderGameOverlayEvent event)
	{
		if (event.getType() == ElementType.FOOD)
		{
			event.setCanceled(true);
			return;
		}
		if (event.getType() == ElementType.HEALTH)
		{
			event.setCanceled(true);
			return;
		}
		if (event.getType() == ElementType.ARMOR)
		{
			event.setCanceled(true);
			return;
		}
		if (event.getType() != ElementType.EXPERIENCE)
		{
			return;
		}

		EntityPlayer player = mc.thePlayer;

		ScaledResolution res = event.getResolution();
		int bottomScreen = res.getScaledHeight(), halfHeight = bottomScreen / 2;
		int rightScreen = res.getScaledWidth(), halfWidth = rightScreen / 2;
		int textHeight = 10, textOffset = 3;
		CapRPGData capAttribute = player.getCapability(CapRPG.CAPABILITY, null);
		CapPlayerExtraData capPlayerExtra = player.getCapability(CapPlayerExtra.CAPABILITY, null);

		int currentbarwidth = 0;
		int xPos = 0;
		int yPos = 0; // + (barH * 2) + 2;
		String s = "";

		// Draw looked at stats
		HTDUtil htdu = new HTDUtil();
		Entity ent = htdu.getEntitesFromLine(player, 10);

		if (ent != null)
		{
			if (ent instanceof EntityLivingBase)
			{
				this.mc.getTextureManager().bindTexture(targetTexture);
				int oBarPosY = 35;
				int oBarWidth = 91;
				int oBarHeight = 12;
				Color textColor = new Color(1f, 0.7f, 0.4f, 0.8f);
				EntityLivingBase living = (EntityLivingBase) ent;
				String livingName = living.getName();
				if (living.hasCapability(CapMobExtra.CAPABILITY, null))
				{
					CapMobExtraData capMobExtra = living.getCapability(CapMobExtra.CAPABILITY, null);
					if (capMobExtra.needsUpdate)
					{
						mc.addScheduledTask(new Runnable() {
							public void run()
							{
								ManagerPacket.INSTANCE.sendToServer(new CapMobUpdatePacket(living.getEntityId()));
							}
						});
						capMobExtra.needsUpdate = false;
					}
					else
					{
						if (capMobExtra.getLevel() != 0) livingName = capMobExtra.getTitle().getTitleName() + " "
								+ livingName + " Lvl " + capMobExtra.getLevel();
						if (capMobExtra.isBoss()) livingName += " *Boss*";
					}
				}
				drawTexturedModalRect(halfWidth - oBarWidth, bottomScreen - oBarPosY, 0, 0, oBarWidth * 2, oBarHeight);
				currentbarwidth = MathHelper.clamp_int(
						(int) ((living.getHealth() / living.getMaxHealth()) * (oBarWidth * 2)), 0, (oBarWidth * 2));
				drawTexturedModalRect(halfWidth - oBarWidth, bottomScreen - oBarPosY, 0, oBarHeight, currentbarwidth,
						oBarHeight);
				if (living.getAbsorptionAmount() > 0)
				{
					currentbarwidth = MathHelper.clamp_int(
							(int) ((living.getAbsorptionAmount() / living.getMaxHealth()) * (oBarWidth * 2)), 0,
							(oBarWidth * 2));
					drawTexturedModalRect(halfWidth - oBarWidth, bottomScreen - oBarPosY, 0, oBarHeight * 2,
							currentbarwidth, oBarHeight);
				}
				drawCText(livingName, halfWidth, bottomScreen - (oBarPosY - 2), textColor.getRGB());
			}
		}

		// Draw Health Bar
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		int middleOffset = 120;
		yPos = bottomScreen - baseH;
		xPos = halfWidth - barW - middleOffset;

		this.mc.getTextureManager().bindTexture(hms_base);
		drawTexturedModalRect(xPos, yPos, 0, 0, this.baseW, this.baseH);

		yPos -= barH;
		this.mc.getTextureManager().bindTexture(healthTexture);

		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		drawTexturedModalRect(xPos, yPos, 0, 0, this.barW, this.barH);

		float adjustedHP = capAttribute.getAdjustedMaxHealth();
		float currentHPPercent = player.getHealth() / player.getMaxHealth();

		currentbarwidth = MathHelper.clamp_int((int) ((currentHPPercent) * this.barW), 0, this.barW);

		drawTexturedModalRect(xPos, yPos, 0, this.barH, currentbarwidth, this.barH);

		// shields
		float currentShieldPercent = player.getAbsorptionAmount() / player.getMaxHealth();
		currentbarwidth = MathHelper.clamp_int((int) ((currentShieldPercent) * this.barW), 0, this.barW);
		drawTexturedModalRect(xPos, yPos, 0, this.barH * 2, currentbarwidth, this.barH);

		s = (int) (adjustedHP * currentHPPercent) + "/" + (int) (adjustedHP);
		drawText(s, xPos + textOffset - ((s.length() - 7) * 2), yPos - textHeight, 0xe62e00);
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);

		// Draw Mana Bar
		yPos = bottomScreen - baseH;
		xPos = halfWidth + middleOffset;

		this.mc.getTextureManager().bindTexture(hms_base);
		drawTexturedModalRect(xPos, yPos, 0, 0, this.baseW, this.baseH);

		yPos -= barH;
		this.mc.getTextureManager().bindTexture(manaTexture);

		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		drawTexturedModalRect(xPos, yPos, 0, 0, this.barW, this.barH);

		float adjustedMana = capAttribute.getAdjustedMaxMana();
		float currentManaPercent = capAttribute.baseStats[ManagerRPGStat.MANA].getValue() / 100f;

		currentbarwidth = MathHelper.clamp_int((int) ((currentManaPercent) * this.barW), 0, this.barW);

		drawTexturedModalRect(xPos, yPos, 0, this.barH, currentbarwidth, this.barH);

		s = (int) (adjustedMana * currentManaPercent) + "/" + (int) (adjustedMana);
		drawText(s, xPos + textOffset - ((s.length() - 7) * 2), yPos - (textHeight * 2) - 3, 0x1a53ff);
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);

		// Draw Stam Bar
		yPos = bottomScreen - baseH - barH;
		this.mc.getTextureManager().bindTexture(staminaTexture);

		float adjustedStam = capAttribute.getAdjustedMaxStam();
		float currentStamPercent = capAttribute.baseStats[ManagerRPGStat.STAM].getValue() / 100f;

		currentbarwidth = MathHelper.clamp_int((int) ((currentStamPercent) * this.barW), 0, this.barW);

		drawTexturedModalRect(xPos, yPos, 0, this.barH, currentbarwidth, this.barH);

		s = (int) (adjustedStam * currentStamPercent) + "/" + (int) (adjustedStam);
		drawText(s, xPos + textOffset - ((s.length() - 7) * 2), yPos - textHeight, 0xdbdd15);
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);

		// Draw Skills
		yPos = bottomScreen - skillHW;
		xPos = halfWidth - barW - middleOffset - skillHW;
		int skillX = 0;
		int skillY = 0;
		int skillTextOffset = 12;

		if (player.isSneaking())
		{
			skillX = capPlayerExtra.playerInfo[capPlayerExtra.SKILL_3] != -1
					? skillHW * capPlayerExtra.playerInfo[capPlayerExtra.SKILL_3] : skillHW;
			skillY = capPlayerExtra.playerInfo[capPlayerExtra.SKILL_3] != -1 ? skillHW : 0;
		}
		else
		{
			skillX = capPlayerExtra.playerInfo[capPlayerExtra.SKILL_1] != -1
					? skillHW * capPlayerExtra.playerInfo[capPlayerExtra.SKILL_1] : skillHW;
			skillY = capPlayerExtra.playerInfo[capPlayerExtra.SKILL_1] != -1 ? skillHW : 0;
		}

		this.mc.getTextureManager().bindTexture(skillTexture);
		drawTexturedModalRect(xPos, yPos, 0, 0, this.skillHW, this.skillHW);
		if (player.isSneaking())
		{
			if (capPlayerExtra.playerInfo[capPlayerExtra.SKILL_3_CD] > 0) GL11.glColor4f(0.25F, 0.25F, 0.25F, 0.75F);
			drawTexturedModalRect(xPos, yPos, skillX, skillY, this.skillHW, this.skillHW);
			drawText(capPlayerExtra.playerInfo[capPlayerExtra.SKILL_3_CD] > 0
					? "" + capPlayerExtra.playerInfo[capPlayerExtra.SKILL_3_CD]
					: KeyHandleEvent.getKeyBindingName(KeyHandleEvent.skill1), xPos + skillTextOffset, yPos + skillTextOffset, 0xffffff);
		}
		else
		{
			if (capPlayerExtra.playerInfo[capPlayerExtra.SKILL_1_CD] > 0) GL11.glColor4f(0.25F, 0.25F, 0.25F, 0.75F);
			drawTexturedModalRect(xPos, yPos, skillX, skillY, this.skillHW, this.skillHW);
			drawText(capPlayerExtra.playerInfo[capPlayerExtra.SKILL_1_CD] > 0
					? "" + capPlayerExtra.playerInfo[capPlayerExtra.SKILL_1_CD]
					: KeyHandleEvent.getKeyBindingName(KeyHandleEvent.skill1), xPos + skillTextOffset, yPos + skillTextOffset, 0xffffff);
		}
		this.mc.getTextureManager().bindTexture(skillTexture);

		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		xPos = halfWidth + middleOffset + barW;

		if (player.isSneaking())
		{
			skillX = capPlayerExtra.playerInfo[capPlayerExtra.SKILL_4] != -1
					? skillHW * capPlayerExtra.playerInfo[capPlayerExtra.SKILL_4] : skillHW;
			skillY = capPlayerExtra.playerInfo[capPlayerExtra.SKILL_4] != -1 ? skillHW : 0;
		}
		else
		{
			skillX = capPlayerExtra.playerInfo[capPlayerExtra.SKILL_2] != -1
					? skillHW * capPlayerExtra.playerInfo[capPlayerExtra.SKILL_2] : skillHW;
			skillY = capPlayerExtra.playerInfo[capPlayerExtra.SKILL_2] != -1 ? skillHW : 0;
		}

		drawTexturedModalRect(xPos, yPos, 0, 0, this.skillHW, this.skillHW);
		if (player.isSneaking())
		{
			if (capPlayerExtra.playerInfo[capPlayerExtra.SKILL_4_CD] > 0) GL11.glColor4f(0.25F, 0.25F, 0.25F, 0.75F);
			drawTexturedModalRect(xPos, yPos, skillX, skillY, this.skillHW, this.skillHW);
			drawText(capPlayerExtra.playerInfo[capPlayerExtra.SKILL_4_CD] > 0
					? "" + capPlayerExtra.playerInfo[capPlayerExtra.SKILL_4_CD]
					: KeyHandleEvent.getKeyBindingName(KeyHandleEvent.skill2), xPos + skillTextOffset, yPos + skillTextOffset, 0xffffff);
		}
		else
		{
			if (capPlayerExtra.playerInfo[capPlayerExtra.SKILL_2_CD] > 0) GL11.glColor4f(0.25F, 0.25F, 0.25F, 0.75F);
			drawTexturedModalRect(xPos, yPos, skillX, skillY, this.skillHW, this.skillHW);
			drawText(capPlayerExtra.playerInfo[capPlayerExtra.SKILL_2_CD] > 0
					? "" + capPlayerExtra.playerInfo[capPlayerExtra.SKILL_2_CD]
					: KeyHandleEvent.getKeyBindingName(KeyHandleEvent.skill2), xPos + skillTextOffset, yPos + skillTextOffset, 0xffffff);
		}

		this.mc.getTextureManager().bindTexture(ICONS);
	}

	private void drawText(String text, int x, int y, int hexColor)
	{
		this.mc.fontRendererObj.drawString(text, x + 1, y, 0);
		this.mc.fontRendererObj.drawString(text, x - 1, y, 0);
		this.mc.fontRendererObj.drawString(text, x, y + 1, 0);
		this.mc.fontRendererObj.drawString(text, x, y - 1, 0);
		this.mc.fontRendererObj.drawString(text, x, y, hexColor);
	}

	private void drawCText(String text, int x, int y, int hexColor)
	{
		drawCenteredString(this.mc.fontRendererObj, text, x, y, hexColor);
	}
}
