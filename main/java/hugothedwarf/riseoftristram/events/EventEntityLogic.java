package hugothedwarf.riseoftristram.events;

import java.util.Random;

import hugothedwarf.riseoftristram.capability.itemextra.CapItemExtra;
import hugothedwarf.riseoftristram.capability.itemextra.CapItemExtraData;
import hugothedwarf.riseoftristram.capability.mobextra.CapMobExtra;
import hugothedwarf.riseoftristram.capability.mobextra.CapMobExtraData;
import hugothedwarf.riseoftristram.capability.playerextra.CapPlayerExtra;
import hugothedwarf.riseoftristram.capability.playerextra.CapPlayerExtraData;
import hugothedwarf.riseoftristram.capability.rpgValues.CapRPG;
import hugothedwarf.riseoftristram.capability.rpgValues.CapRPGData;
import hugothedwarf.riseoftristram.libs.HTDUtil;
import hugothedwarf.riseoftristram.libs.ModValues;
import hugothedwarf.riseoftristram.managers.ManagerRPGStat;
import hugothedwarf.riseoftristram.managers.ManagerLoot;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityArmorStand;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EntityDamageSource;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingJumpEvent;
import net.minecraftforge.event.entity.living.LivingFallEvent;
import net.minecraftforge.event.entity.living.LivingHealEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class EventEntityLogic
{

	@SubscribeEvent
	public void onPlayerClone(PlayerEvent.Clone e)
	{
		if (e.isWasDeath())
		{
			e.getEntityPlayer().getCapability(CapPlayerExtra.CAPABILITY, null)
					.decompressData(e.getOriginal().getCapability(CapPlayerExtra.CAPABILITY, null).compressData());
			e.getEntityPlayer().getCapability(CapRPG.CAPABILITY, null).baseStats = e.getOriginal()
					.getCapability(CapRPG.CAPABILITY, null).baseStats;
		}
	}

	@SubscribeEvent
	public void onEntityHurt(LivingHurtEvent e)
	{
		Entity entity = e.getEntity();
		EntityLivingBase entityLiving = (EntityLivingBase) entity;
		DamageSource dSource = e.getSource();
		float finalDamage = e.getAmount();
		int baseDamage = 1;
		Random rand = new Random();
		CapRPGData capAttributeHurt = null;
		CapRPGData capAttributeAttacker = null;
		String type = e.getSource().getDamageType();

		if (e.getAmount() > 0)
		{
			if (entity != null && entity instanceof EntityLivingBase
					&& entity.hasCapability(CapRPG.CAPABILITY, null))
			{
				capAttributeHurt = entity.getCapability(CapRPG.CAPABILITY, null);
				capAttributeHurt.gotHit();
			}
			finalDamage *= ModValues.BASE_UPSCALE;
			baseDamage *= ModValues.BASE_UPSCALE;

			// if damage was unblockable
			if (dSource.isUnblockable() || dSource.isExplosion())
			{
				capAttributeHurt.getAdjustedDamagePercent(finalDamage);
			}
			else // Entity was attacked
			{
				if (dSource instanceof EntityDamageSource)
				{
					EntityDamageSource source = (EntityDamageSource) dSource;
					if (source.getSourceOfDamage() != null && source.getSourceOfDamage() instanceof EntityLivingBase)
					{
						EntityLivingBase attacker = (EntityLivingBase) source.getSourceOfDamage();
						float minDamage = ModValues.BASE_MIN_DAMAGE + ModValues.MIN_DAMAGE;
						float maxDamage = ModValues.BASE_MAX_DAMAGE + ModValues.MAX_DAMAGE;

						if (attacker != null && attacker instanceof EntityLivingBase
								&& attacker.hasCapability(CapRPG.CAPABILITY, null))
						{
							capAttributeAttacker = attacker.getCapability(CapRPG.CAPABILITY, null);
							if (attacker instanceof EntityPlayer)
							{
								CapPlayerExtraData capPlayerExtraAttack = attacker
										.getCapability(CapPlayerExtra.CAPABILITY, null);
								if (capPlayerExtraAttack.getCurrentClass() != null)
								{
									minDamage += capPlayerExtraAttack.getCurrentClass().getMinDmg(capAttributeAttacker);
									maxDamage += capPlayerExtraAttack.getCurrentClass().getMaxDmg(capAttributeAttacker);
								}
							}
							else
							{
								int minMaxBoost = (capAttributeAttacker.baseStats[ManagerRPGStat.STRENGTH].getValueInt()
										+ capAttributeAttacker.baseStats[ManagerRPGStat.DEXTERITY].getValueInt()
										+ capAttributeAttacker.baseStats[ManagerRPGStat.INTELLIGENCE].getValueInt()) / 6;
								minDamage += minMaxBoost;
								maxDamage += minMaxBoost;
							}

							float dmgBonus = 1f;

							// if (type.equals("arrow"))
							// dmgBonus = (capAttributeAttacker.stats[ManagerCapStat.DEXTERITY].getValue() + 100)
							// / 100;
							// else if (type.equals("fireball") || type.equals("magic"))
							// dmgBonus = (capAttributeAttacker.stats[ManagerCapStat.INTELLIGENCE].getValue() + 100)
							// / 100;
							// else dmgBonus = (capAttributeAttacker.stats[ManagerCapStat.STRENGTH].getValue() + 100)
							// / 100;

							maxDamage = maxDamage < minDamage ? minDamage : maxDamage;
							finalDamage += ((rand.nextInt((int) maxDamage - (int) minDamage) + minDamage) * dmgBonus);
							finalDamage *= capAttributeAttacker.baseStats[ManagerRPGStat.ENHANCED_DAMAGE].getValue() != 0
									? (1 + (capAttributeAttacker.baseStats[ManagerRPGStat.ENHANCED_DAMAGE].getValue()
											/ 100f))
									: 1;
						}
					}
				}
			}
			// Get who got hurt, if someone hurt them, and if that someone is a player?

			// Defence
			float newDamage = finalDamage;
			if (!dSource.isUnblockable() || dSource instanceof EntityDamageSource)
			{
				float defRate = capAttributeHurt.baseStats[ManagerRPGStat.DEFENCE_RATING].getValue();
				float percentReduction = finalDamage * (MathHelper.clamp_float((defRate * ModValues.DEF_REDUCTION_RATE) / 100f, 0, ModValues.DEF_MAX_PERCENT_REDUCTION));
				float flatReduction = defRate > ModValues.DEF_RATE_OVERLOAD
						? ((defRate - ModValues.DEF_RATE_OVERLOAD) * ModValues.DEF_FLAT_REDUCTION_OVERLOAD) : 0;
				newDamage -= (percentReduction + flatReduction);
				if (!(entity instanceof EntityPlayer))
				{
					entityLiving.hurtResistantTime = 1;
				}
			}

			// lifesteal and manasteal need to go here, since the dmg was adjusted.

			newDamage = entityLiving.getMaxHealth() * (capAttributeHurt.getAdjustedDamagePercent(newDamage));
			finalDamage = newDamage < 0 ? 0.01f : newDamage;
			e.setAmount(finalDamage);
		}

	}

	@SubscribeEvent
	public void onEntityHeal(LivingHealEvent e)
	{
		if (e.getEntityLiving() != null)
		{
			EntityLivingBase ent = e.getEntityLiving();
			CapRPGData cap = ent.getCapability(CapRPG.CAPABILITY, null);

			float adjustedMaxHealth = cap.getAdjustedMaxHealth();
			float adjustedHeal = ent.getMaxHealth()
					* (((adjustedMaxHealth + (e.getAmount() * ModValues.BASE_UPSCALE)) / adjustedMaxHealth) - 1f);
			e.setAmount(adjustedHeal);

		}
	}

	@SubscribeEvent
	public void onEntityUpdate(LivingEvent.LivingUpdateEvent event)
	{
		EntityLivingBase ent = event.getEntityLiving();
		if (ent != null && !(ent instanceof EntityPlayer) && !(ent instanceof EntityArmorStand))
		{
			if (!ent.worldObj.isRemote)
			{
				if (ent.hasCapability(CapMobExtra.CAPABILITY, null) && HTDUtil.entityCanAttack(ent))
				{
					CapMobExtraData capMobExtra = ent.getCapability(CapMobExtra.CAPABILITY, null);
					// if (capMobExtra.getLevel() == 0 && ent instanceof EntityLiving)
					// {
					// EntityCreature entL = (EntityCreature) ent;
					// entL.targetTasks.addTask(2, new EntityAINearestAttackableTarget(entL, EntityLiving.class, true));
					// }

					if (capMobExtra.getLevel() == 0) capMobExtra.rollStats();
					// if (capMobExtra.isBoss())
					// {
					// ent.addPotionEffect(new PotionEffect(MobEffects.REGENERATION, 120, 1));
					// ent.addPotionEffect(new PotionEffect(MobEffects.GLOWING, 120));
					// }
				}
			}

			if (ent.worldObj.getWorldTime() % 20 == 0)
			{
				if (ent.hasCapability(CapRPG.CAPABILITY, null))
				{
					// need to include shields at some point too
					CapRPGData capAttribute = ent.getCapability(CapRPG.CAPABILITY, null);
					if (capAttribute.canRegenHP()) ent.heal(ent.getMaxHealth()
							* ((0.5f / 100f) + (capAttribute.baseStats[ManagerRPGStat.BONUS_HP_REGEN].getValue() / 100f)));
				}
				else ent.heal(ent.getMaxHealth() * (0.5f / 100f));
			}
		}
	}

	@SubscribeEvent
	public void onEntityDied(LivingDeathEvent e)
	{
		// if (e.getEntity() != null &&
		// e.getEntity().hasCapability(CapAttribute.CAPABILITY, null)
		// && !e.getEntity().worldObj.isRemote)
		// {
		// Attribute[] attrs =
		// e.getEntity().getCapability(CapAttribute.CAPABILITY,
		// null).attributes;
		// Console.out().println("Dead Mob: " + e.getEntity().getName() + " :::
		// "
		// + (e.getEntity().worldObj.isRemote ? "client" : "server"));
		// for (int index = 0; index < attrs.length; index++)
		// {
		// Console.out().println(attrs[index].getName() + "=" +
		// attrs[index].getValue());
		// }
		// Console.out().println("-----------------------------------------");
		// }
	}

	@SubscribeEvent
	public void onEntityDrop(LivingDropsEvent e)
	{
		if (HTDUtil.entityCanAttack(e.getEntity()) && e.getSource().getEntity() instanceof EntityPlayer)
		{
			EntityItem[] newLoot = ManagerLoot.addLoot(e.getEntity());
			if (newLoot != null)
			{
				for (EntityItem item : newLoot)
				{
					e.getDrops().add(item);
				}
			}
			if (!e.getDrops().isEmpty() && e.getDrops().size() > 0)
			{
				CapMobExtraData capMobExtra = e.getEntity().getCapability(CapMobExtra.CAPABILITY, null);
				ItemStack item;
				for (int i = 0; i < e.getDrops().size(); i++)
				{
					item = e.getDrops().get(i).getEntityItem();
					if (HTDUtil.isItemEquipment(item.getItem()))
					{
						CapItemExtraData capItemExtra = item.getCapability(CapItemExtra.CAPABILITY, null);
						capItemExtra.setLevel(
								capMobExtra.getLevel() + (capMobExtra.isBoss() ? ModValues.BOSS_LEVEL_BONUS : 0));
						capItemExtra.rollStats(item, capMobExtra.isBoss(), false);
					}
				}
			}
		}
	}

	@SubscribeEvent
	public void onEntityFall(LivingFallEvent e)
	{

	}

	@SubscribeEvent
	public void onEntityJump(LivingJumpEvent e)
	{
		// float jumpStamCost = 20f;
		// EntityLivingBase entity = e.getEntityLiving();
		// if(entity instanceof EntityPlayer)
		// {
		// CapAttributeData cap = entity.getCapability(CapAttribute.CAPABILITY,
		// null);
		// if(!cap.canConsumeStam(jumpStamCost))
		// {
		// cap.consumeStam(jumpStamCost);
		// entity.motionY *= 0.02f;
		// }
		// }
	}
}
