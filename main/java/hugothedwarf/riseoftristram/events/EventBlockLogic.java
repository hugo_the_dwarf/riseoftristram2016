package hugothedwarf.riseoftristram.events;

import hugothedwarf.riseoftristram.capability.playerextra.CapPlayerExtra;
import hugothedwarf.riseoftristram.capability.playerextra.CapPlayerExtraData;
import hugothedwarf.riseoftristram.managers.ManagerPlayerClassProfession;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyInteger;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.entity.player.PlayerEvent.BreakSpeed;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class EventBlockLogic
{

	@SubscribeEvent
	public void BlockBreakSpeed(BreakSpeed e)
	{
		Material blockMat = e.getState().getMaterial();
		Entity ent = e.getEntity();
		if (ent instanceof EntityPlayer)
		{
			EntityPlayer player = (EntityPlayer) ent;
			CapPlayerExtraData capPlayerExtra = player.getCapability(CapPlayerExtra.CAPABILITY, null);
			if (capPlayerExtra.getCurrentProfessionId() != -1)
			{
				String professionName = ManagerPlayerClassProfession.professions[capPlayerExtra.getCurrentProfessionId()].professionName;
				if (professionName == ManagerPlayerClassProfession.professionMiner)
				{
					if (blockMat == Material.ROCK || blockMat == Material.CLAY || blockMat == Material.SAND) e.setNewSpeed(e.getNewSpeed() + 3f);
				}
				else if (professionName == ManagerPlayerClassProfession.professionFarmer)
				{
					if (blockMat == Material.GRASS || blockMat == Material.WOOD || blockMat == Material.PLANTS || blockMat == Material.GROUND || blockMat == Material.LEAVES)
						e.setNewSpeed(e.getNewSpeed() + 3f);
				}
			}
		}
	}

	@SubscribeEvent
	public void BlockBreak(BlockEvent.HarvestDropsEvent e)
	{
		if (e.getHarvester() instanceof EntityPlayer)
		{
			EntityPlayer player = (EntityPlayer) e.getHarvester();
			CapPlayerExtraData capPlayerExtra = player.getCapability(CapPlayerExtra.CAPABILITY, null);
			Block block = e.getState().getBlock();
			if (capPlayerExtra.getCurrentProfessionId() != -1)
			{
				String professionName = ManagerPlayerClassProfession.professions[capPlayerExtra.getCurrentProfessionId()].professionName;
				if (professionName.equals(ManagerPlayerClassProfession.professionMiner))
				{
					if (!e.isSilkTouching())
					{
						if (block == Blocks.COAL_ORE || block == Blocks.REDSTONE_ORE || block == Blocks.DIAMOND_ORE || block == Blocks.EMERALD_ORE)
						{
							for (ItemStack is : e.getDrops())
							{
								is.stackSize += (e.getWorld().rand.nextInt(4) + 1);
							}
						}
					}
				}
				else if (professionName.equals(ManagerPlayerClassProfession.professionFarmer))
				{
					int age = -1;
					int maxAge = -1;
					for (IProperty<?> p : e.getState().getPropertyNames())
					{
						if (p.getName().toLowerCase().equals("age"))
						{
							if (p instanceof PropertyInteger)
							{
								PropertyInteger ageProp = (PropertyInteger) p;
								age = e.getState().getValue(ageProp).intValue();
								maxAge = ageProp.getAllowedValues().size() - 1;
								break;
							}
						}
					}

					if (age != -1)
					{
						if (age == maxAge)
						{
							for (ItemStack is : e.getDrops())
							{
								is.stackSize += (e.getWorld().rand.nextInt(4) + 1);
							}
						}
					}
					// /End of Farmer stuff
				}
				// End of profession checks
			}
		}
	}

}
