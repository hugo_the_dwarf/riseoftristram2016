package hugothedwarf.riseoftristram.events;

import hugothedwarf.riseoftristram.capability.playerextra.CapPlayerExtra;
import hugothedwarf.riseoftristram.capability.playerextra.CapPlayerExtraData;
import hugothedwarf.riseoftristram.capability.rpgValues.CapRPG;
import hugothedwarf.riseoftristram.capability.rpgValues.CapRPGData;
import hugothedwarf.riseoftristram.comms.packets.ManaStamUpdatePacket;
import hugothedwarf.riseoftristram.comms.packets.capabilities.CapAttributeResponsePacket;
import hugothedwarf.riseoftristram.comms.packets.capabilities.CapPlayerExtraResponsePacket;
import hugothedwarf.riseoftristram.libs.HTDUtil;
import hugothedwarf.riseoftristram.libs.ModValues;
import hugothedwarf.riseoftristram.managers.ManagerRPGStat;
import hugothedwarf.riseoftristram.managers.ManagerPacket;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.init.MobEffects;
import net.minecraft.item.ItemShield;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.FoodStats;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

public class FMLEventPlayerTick
{

	@SubscribeEvent
	public void playerTickEvent(TickEvent.PlayerTickEvent event)
	{
		EntityPlayer player = event.player;
		CapRPGData capAttribute = player.getCapability(CapRPG.CAPABILITY, null);
		CapPlayerExtraData capPlayerExtra = player.getCapability(CapPlayerExtra.CAPABILITY, null);
		boolean remote = player.worldObj.isRemote;

		// 1/2 Second
		// if (player.worldObj.getWorldTime() % 10 == 0)
		if (player.ticksExisted % 10 == 0)
		{
			// Hunger
			handleHunger(player);
			if (player.isSprinting() && !player.isInWater() && player.onGround)
			{
				if (capAttribute.canConsumeStam(ModValues.SPRINT_STAM_COST))
					capAttribute.consumeStam(ModValues.SPRINT_STAM_COST);
				else
				{
					player.setSprinting(false);
					player.addPotionEffect(new PotionEffect(MobEffects.HUNGER, 60));
				}
			}

			// Running
		}

		// 1 Second
		// if (player.worldObj.getWorldTime() % 20 == 0)
		if (player.ticksExisted % 20 == 0)
		{
			// ServerSide
			if (!remote)
			{
				capAttribute.update();
				for (int i = capPlayerExtra.SKILL_1_CD; i <= capPlayerExtra.SKILL_4_CD; i++)
				{
					if (capPlayerExtra.playerInfo[i] > 0) capPlayerExtra.playerInfo[i] -= 1;
				}

				if (player.shouldHeal() && capAttribute.canRegenHP())
				{
					player.heal(player.getMaxHealth()
							* ((1f / 100f) + (capAttribute.baseStats[ManagerRPGStat.BONUS_HP_REGEN].getValue() / 100f)
									+ (capAttribute.baseStats[ManagerRPGStat.VITALITY].getValue() / 50f)));
				}

				player.getServer().addScheduledTask(new Runnable() {
					public void run()
					{
						ManagerPacket.INSTANCE
								.sendTo(new ManaStamUpdatePacket(capAttribute.baseStats[ManagerRPGStat.MANA].getValue(),
										capAttribute.baseStats[ManagerRPGStat.STAM].getValue()), (EntityPlayerMP) player);
						ManagerPacket.INSTANCE.sendTo(new CapPlayerExtraResponsePacket(capPlayerExtra.compressData()),
								(EntityPlayerMP) player);
					}
				});
			}
		}

		// 3 Second
		// if (!remote && player.worldObj.getWorldTime() % 60 == 0)
		if (!remote && player.ticksExisted % 30 == 0)
		{

			handleStats(player.inventory, capAttribute);
			player.getServer().addScheduledTask(new Runnable() {
				public void run()
				{
					ManagerPacket.INSTANCE.sendTo(new CapAttributeResponsePacket(capAttribute.getStatValues()),
							(EntityPlayerMP) player);
					ManagerPacket.INSTANCE.sendTo(new CapPlayerExtraResponsePacket(capPlayerExtra.compressData()),
							(EntityPlayerMP) player);
				}
			});
		}

		// // Auto Repair
		// if (player.worldObj.getWorldTime() % (20 * 3) == 0)
		// fixInvetoryItems(player.inventory);
	}

	private void handleHunger(EntityPlayer player)
	{
		FoodStats fs = player.getFoodStats();

		if (!player.isPotionActive(MobEffects.HUNGER))
		{
			if (fs.getFoodLevel() < 8)
			{
				fs.setFoodLevel(8);
			}
			else if (fs.getFoodLevel() > 8)
			{
				float healAmount = fs.getFoodLevel() - 8;
				player.heal(healAmount * 2);
				fs.setFoodLevel(8);
			}
		}
		else
		{
			if (fs.getFoodLevel() < 1)
			{
				fs.setFoodLevel(1);
			}
			else if (fs.getFoodLevel() > 1)
			{
				float healAmount = fs.getFoodLevel() - 1;
				player.heal(healAmount);
				fs.setFoodLevel(1);
			}
		}
	}

	private void handleStats(InventoryPlayer inv, CapRPGData playerStats)
	{
		float[] stats = new float[ManagerRPGStat.STAT_NAMES.length];

		// Add Player Class Bonuses
		CapPlayerExtraData capPlayer = inv.player.getCapability(CapPlayerExtra.CAPABILITY, null);

		for (int index = 0; index < stats.length; index++)
		{
			stats[index] = 0;
		}

		ItemStack[] items = new ItemStack[6];
		int indexer = 0;

		for (ItemStack i : inv.armorInventory)
		{
			if (i != null) items[indexer++] = i;
		}

		if (inv.mainInventory[inv.currentItem] != null
				&& HTDUtil.isItemWeapon(inv.mainInventory[inv.currentItem].getItem()))
			items[indexer++] = inv.mainInventory[inv.currentItem];
		if (inv.offHandInventory[0] != null && (inv.offHandInventory[0].getItem() instanceof ItemShield
				|| HTDUtil.isItemWeapon(inv.offHandInventory[0].getItem())))
			items[indexer++] = inv.offHandInventory[0];

		CapRPGData capAttribute = null;
		for (ItemStack i : items)
		{
			if (i != null)
			{
				capAttribute = i.getCapability(CapRPG.CAPABILITY, null);
				if (capAttribute != null)
				{
					for (int index = 0; index < stats.length; index++)
					{
						stats[index] += capAttribute.baseStats[index].getValue();
					}
				}
			}
		}

		stats[ManagerRPGStat.MANA] = playerStats.baseStats[ManagerRPGStat.MANA].getValue();
		stats[ManagerRPGStat.STAM] = playerStats.baseStats[ManagerRPGStat.STAM].getValue();

		playerStats.setStatValues(stats);
	}

	private void fixInvetoryItems(InventoryPlayer inv)
	{
		boolean hasFixed = false;
		ItemStack is = inv.getCurrentItem();
		for (int repairs = 0; repairs < (1 + (inv.player.experienceLevel / 4)); repairs++)
		{
			if (!hasFixed && is != null && is.isItemDamaged())
			{
				is.setItemDamage(is.getItemDamage() - 1);
				hasFixed = true;
			}

			is = inv.offHandInventory[0];
			if (!hasFixed && is != null && is.isItemDamaged())
			{
				is.setItemDamage(is.getItemDamage() - 1);
				hasFixed = true;
			}

			if (!hasFixed)
			{
				for (ItemStack isa : inv.armorInventory)
				{
					if (isa != null && isa.isItemDamaged())
					{
						isa.setItemDamage(isa.getItemDamage() - 1);
						hasFixed = true;
						break;
					}
				}
			}

			if (!hasFixed)
			{
				for (ItemStack ism : inv.mainInventory)
				{
					if (ism != null && ism.isItemDamaged())
					{
						ism.setItemDamage(ism.getItemDamage() - 1);
						hasFixed = true;
						break;
					}
				}
			}
		}
	}

}
