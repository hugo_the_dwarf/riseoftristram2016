package hugothedwarf.riseoftristram.events;

import java.util.Random;

import hugothedwarf.riseoftristram.capability.itemextra.CapItemExtra;
import hugothedwarf.riseoftristram.capability.itemextra.CapItemExtraData;
import hugothedwarf.riseoftristram.capability.rpgValues.CapRPG;
import hugothedwarf.riseoftristram.capability.rpgValues.CapRPGData;
import hugothedwarf.riseoftristram.comms.packets.capabilities.CapItemUpdatePacket;
import hugothedwarf.riseoftristram.libs.HTDUtil;
import hugothedwarf.riseoftristram.managers.ManagerRPGStat;
import hugothedwarf.riseoftristram.managers.ManagerPacket;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.ItemCraftedEvent;

public class EventItemLogic
{

	@SubscribeEvent
	public void onItemCrafted(ItemCraftedEvent e)
	{
		if (!e.player.worldObj.isRemote && HTDUtil.isItemEquipment(e.crafting.getItem()))
		{
			Random rand = new Random();
			CapItemExtraData capItemExtra = e.crafting.getCapability(CapItemExtra.CAPABILITY, null);
			capItemExtra.setLevel(rand.nextInt(20) + 1);
			capItemExtra.rollStats(e.crafting, false, true);
		}
	}

	@SubscribeEvent(priority = EventPriority.HIGHEST)
	public void onItemToolTipUpdate(ItemTooltipEvent e)
	{
		try
		{
			if (e.getItemStack().hasCapability(CapRPG.CAPABILITY, null))
			{
				CapRPGData capAttribute = e.getItemStack().getCapability(CapRPG.CAPABILITY, null);
				CapItemExtraData capItemExtra = e.getItemStack().getCapability(CapItemExtra.CAPABILITY, null);

				if (!capAttribute.needsUpdate)
				{
					if (capItemExtra.getLevel() != -1) e.getToolTip().add("Item Level: " + capItemExtra.getLevel());
					if (capItemExtra.getQuality() != null)
						e.getToolTip().add("Quality: " + capItemExtra.getQuality().getName());

					for (int index = 0; index < capAttribute.baseStats.length; index++)
					{
						if (capAttribute.baseStats[index].getValue() != 0)
						{
							String newLine = ManagerRPGStat.isStatPercent(index)
									? capAttribute.baseStats[index].getName() + ": "
											+ capAttribute.baseStats[index].getValuePercent()
									: capAttribute.baseStats[index].getName() + ": " + (int)capAttribute.baseStats[index].getValue();
							e.getToolTip().add(newLine);
						}
					}

					if (capItemExtra.getSockets() > 0) e.getToolTip()
							.add("Sockets: " + capItemExtra.getSocketsUsed() + "/" + capItemExtra.getSockets());
				}
				else
				{
					e.getToolTip().add("Unidentified...");

					EntityPlayer player = e.getEntityPlayer();
					if (player != null)
					{
						int foundInventoryType = -1;
						int foundSlotId = -1;
						InventoryPlayer inv = player.inventory;
						ItemStack is = null;

						for (int type = 0; type < 3; type++)
						{
							switch (type)
							{
								case 0:
									for (int index = 0; index < inv.mainInventory.length; index++)
									{
										is = inv.mainInventory[index];
										if (is != null && is == e.getItemStack())
										{
											foundInventoryType = type;
											foundSlotId = index;
											capAttribute.needsUpdate = false;
										}
									}
									break;
								case 1:
									for (int index = 0; index < inv.offHandInventory.length; index++)
									{
										is = inv.offHandInventory[index];
										if (is != null && is == e.getItemStack())
										{
											foundInventoryType = type;
											foundSlotId = index;
											capAttribute.needsUpdate = false;
										}
									}
									break;
								case 2:
									for (int index = 0; index < inv.armorInventory.length; index++)
									{
										is = inv.armorInventory[index];
										if (is != null && is == e.getItemStack())
										{
											foundInventoryType = type;
											foundSlotId = index;
											capAttribute.needsUpdate = false;
										}
									}
									break;
							}
						}

						int inventoryType = foundInventoryType, slotId = foundSlotId;
						Minecraft.getMinecraft().addScheduledTask(new Runnable() {
							public void run()
							{
								if (inventoryType != -1 && slotId != -1)
								{
									ManagerPacket.INSTANCE.sendToServer(new CapItemUpdatePacket(inventoryType, slotId));
								}
							}
						});

					}

					// cap.needsUpdate = false;
				}
			}
		}
		catch (Exception ex)
		{
			System.out.println("Item Tooltip Error");
			System.out.println(ex.getMessage());
			System.out.println(ex.getStackTrace());
		}
	}

}
