package hugothedwarf.riseoftristram.events;

import hugothedwarf.riseoftristram.capability.itemextra.CapItemExtraProvider;
import hugothedwarf.riseoftristram.capability.mobextra.CapMobExtraProvider;
import hugothedwarf.riseoftristram.capability.playerextra.CapPlayerExtraProvider;
import hugothedwarf.riseoftristram.capability.rpgValues.CapRPGProvider;
import hugothedwarf.riseoftristram.libs.HTDUtil;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.passive.EntityVillager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class EventAddCapabilities
{
	@SubscribeEvent
	public void onAddCapabilitiesEntity(AttachCapabilitiesEvent<Entity> e)
	{
		if (canHaveAttributes(e.getObject()))
		{
			EntityLivingBase ent = (EntityLivingBase) e.getObject();

			if (ent instanceof EntityPlayer)
				e.addCapability(CapPlayerExtraProvider.KEY, new CapPlayerExtraProvider(ent));
			else if (!(ent instanceof EntityVillager))
				e.addCapability(CapMobExtraProvider.KEY, new CapMobExtraProvider(ent));

			e.addCapability(CapRPGProvider.KEY, new CapRPGProvider(ent));
		}
	}

	@SubscribeEvent
	public void onAddCapabilitiesItemStack(AttachCapabilitiesEvent<Item> e)
	{
		if (HTDUtil.isItemEquipment(e.getObject()))
		{
			e.addCapability(CapRPGProvider.KEY, new CapRPGProvider(e.getObject()));
			e.addCapability(CapItemExtraProvider.KEY, new CapItemExtraProvider(e.getObject()));
		}
	}

	public static boolean canHaveAttributes(Entity entity)
	{
		if (entity instanceof EntityLivingBase) return true;
		return false;
	}
}
