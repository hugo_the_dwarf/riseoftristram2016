package hugothedwarf.riseoftristram.capability.rpgValues;

import java.util.concurrent.Callable;

import net.minecraft.nbt.NBTBase;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.CapabilityManager;

public class CapRPG
{
	@CapabilityInject(CapRPGData.class)
	public static Capability<CapRPGData> CAPABILITY = null;

	public static void register()
	{
		CapabilityManager.INSTANCE.register(CapRPGData.class, new StorageHelper(), new DefaultInstanceFactory());
	}

	public static class StorageHelper implements Capability.IStorage<CapRPGData>
	{

		@Override
		public NBTBase writeNBT(Capability<CapRPGData> capability, CapRPGData instance, EnumFacing side)
		{
			return instance.writeData();
		}

		@Override
		public void readNBT(Capability<CapRPGData> capability, CapRPGData instance, EnumFacing side, NBTBase nbt)
		{
			instance.readData(nbt);
		}
	}

	public static class DefaultInstanceFactory implements Callable<CapRPGData>
	{
		@Override
		public CapRPGData call() throws Exception
		{
			return new CapRPGData();
		}
	}
}
