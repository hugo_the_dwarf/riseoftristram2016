package hugothedwarf.riseoftristram.capability.rpgValues;

import hugothedwarf.riseoftristram.libs.componets.Stat;
import hugothedwarf.riseoftristram.libs.skills.ISkill;
import hugothedwarf.riseoftristram.managers.ManagerPlayerClassProfession;
import hugothedwarf.riseoftristram.managers.ManagerRPGStat;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;

public class CapRPGData
{
	public static final float MAX_STAM_MANA = 100f;
	public static final String BASE_STAT_PREFIX = "base_";
	public static final String LEVEL_STAT_PREFIX = "level_";
	public static final String SPECIAL_STAT_PREFIX = "special_";

	//Can use Base Stats or Special Stats for Item Socketted Values
	public Stat[] baseStats; // For the player and mobs, class stats
	public Stat[] levelStats; // Player chosen stats to upgrade, Item Stats
	public Stat[] specialStats; // not needed?

	public ISkill[] skills; // When a Class is Set, Skills will be given out
	// Tasks or Effects this needs to be created later on
	// Owner GUID NBT saving
	// Minion List (GUIDs) NBT saving
	// Loaded Minion List (loads from GUIDs) no NBT saving loaded after the read

	public static final int HP_REGEN_DELAY = 0, MP_REGEN_DELAY = 1, SP_REGEN_DELAY = 2;
	public int[] delays = new int[]
	{
		0,
		0,
		0
	};

	public int level = 1;
	public int skillPoints = 0;
	
	public int classID = -1;
	public int proffID = -1;	
	
	public int itemQualityID = -1;
	public int mobTypeID = -1; //0 is normal, 1 is elite, 2 is unique, 3 is boss, -1 is non-mob

	public boolean needsUpdate = true;
	
	public float getModifiedMaxHealth()
	{
		float maxHP = 0;
		float vit = getStatValue(ManagerRPGStat.VITALITY);
		float bonusHP = getStatValue(ManagerRPGStat.BONUS_HP);
		if (classID != -1)
		{
			try
			{
				maxHP += vit * ManagerPlayerClassProfession.classes[classID].getHpPerVit();
			}
			catch(Exception e)
			{
				classID = -1;
			}
		}
		else
		{
			maxHP += vit * 1.25f;
		}
		return maxHP += bonusHP;		 
	}

	

	public void update()
	{
		for (int i = 0; i < delays.length; i++)
		{
			if (delays[i] > 0) delays[i]--;
		}
		if (skills != null)
		{
			for (int i = 0; i < skills.length; i++)
			{
				skills[i].update();
			}
		}
	}

	public Stat[] getCurrentStats()
	{
		Stat[] outputStats = new Stat[ManagerRPGStat.STAT_TAGS.length];
		for (int i = 0; i < ManagerRPGStat.STAT_TAGS.length; i++)
		{
			outputStats[i].setValue(baseStats[i].getValue() + levelStats[i].getValue() + specialStats[i].getValue());
		}
		return outputStats;
	}

	public float getStatValue(int statID)
	{
		return (baseStats[statID].getValue() + levelStats[statID].getValue() + specialStats[statID].getValue());
	}

	public CapRPGData()
	{
		baseStats = ManagerRPGStat.returnEmptyStatMatrix();
		levelStats = ManagerRPGStat.returnEmptyStatMatrix();
		specialStats = ManagerRPGStat.returnEmptyStatMatrix();
	}

	public NBTBase writeData()
	{
		NBTTagCompound tag = new NBTTagCompound();
		tag = ManagerRPGStat.writeStatData(tag, baseStats, BASE_STAT_PREFIX);
		tag = ManagerRPGStat.writeStatData(tag, levelStats, LEVEL_STAT_PREFIX);
		tag = ManagerRPGStat.writeStatData(tag, specialStats, SPECIAL_STAT_PREFIX);
		for (int i = 0; i < delays.length; i++)
		{
			tag.setInteger("delay_" + i, delays[i]);
		}
		tag.setInteger("classid", classID);
		tag.setInteger("proffid", proffID);
		tag.setInteger("itemquality", itemQualityID);
		return tag;
	}

	public void readData(NBTBase nbt)
	{
		NBTTagCompound tag = (NBTTagCompound) nbt;
		baseStats = ManagerRPGStat.readStatData(tag, BASE_STAT_PREFIX);
		levelStats = ManagerRPGStat.readStatData(tag, LEVEL_STAT_PREFIX);
		specialStats = ManagerRPGStat.readStatData(tag, SPECIAL_STAT_PREFIX);
		for (int i = 0; i < delays.length; i++)
		{
			delays[i] = tag.getInteger("delay_" + i);
		}
		classID = tag.getInteger("classid");
		proffID = tag.getInteger("proffid");
		itemQualityID = tag.getInteger("itemquality");
	}
}
