package hugothedwarf.riseoftristram.capability.rpgValues;

import hugothedwarf.riseoftristram.RiseOfTristram;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;

public class CapRPGProvider implements ICapabilityProvider, ICapabilitySerializable<NBTTagCompound>
{

	public static final ResourceLocation KEY = new ResourceLocation(RiseOfTristram.MODID, "capability_rpg");

	private CapRPGData INSTANCE = new CapRPGData();

	public CapRPGProvider()
	{}

	@Override
	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
	{
		return capability == CapRPG.CAPABILITY;
	}

	@Override
	public <T> T getCapability(Capability<T> capability, EnumFacing facing)
	{
		if (capability == CapRPG.CAPABILITY) return (T) INSTANCE;
		return null;
	}

	@Override
	public NBTTagCompound serializeNBT()
	{
		return (NBTTagCompound) CapRPG.CAPABILITY.writeNBT(INSTANCE, null);
	}

	@Override
	public void deserializeNBT(NBTTagCompound nbt)
	{
		CapRPG.CAPABILITY.readNBT(INSTANCE, null, nbt);
	}

}
