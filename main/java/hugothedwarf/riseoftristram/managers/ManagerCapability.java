package hugothedwarf.riseoftristram.managers;

import hugothedwarf.riseoftristram.capability.rpgValues.CapRPG;

public class ManagerCapability
{
	public ManagerCapability()
	{

	}

	public static void registerCapabilities()
	{
		CapRPG.register();
	}
}
