package hugothedwarf.riseoftristram.managers;

import java.util.ArrayList;

import hugothedwarf.riseoftristram.libs.componets.Stat;
import net.minecraft.nbt.NBTTagCompound;

public class ManagerRPGStat
{
	public static final int STRENGTH = 0, DEXTERITY = 1, INTELLIGENCE = 2, VITALITY = 3, MIN_DAMAGE = 4, MAX_DAMAGE = 5,
			DEFENCE_RATING = 6, LIFE_STEAL = 7, MANA_STEAL = 8, ENHANCED_DAMAGE = 9, BONUS_HP = 10, BONUS_MANA = 11,
			BONUS_STAM = 12, BONUS_HP_REGEN = 13, BONUS_MANA_REGEN = 14, BONUS_STAM_REGEN = 15, MANA = 16, STAM = 17;

	public static final String[] STAT_TAGS = new String[]
	{
		"Str",
		"Dex",
		"Int",
		"Vit",
		"MinDmg",
		"MaxDmg",
		"DefRating",
		"LS",
		"MS",
		"DmgBV",
		"HPBV",
		"MPBV",
		"SPBV",
		"HPReg",
		"MPReg",
		"SPReg",
		"MP",
		"SP"
	};
	public static final String[] STAT_NAMES = new String[]
	{
		"Strength",
		"Dexterity",
		"Intelligence",
		"Vitality",
		"Min Damage",
		"Max Damage",
		"Defence Rating",
		"Life Steal",
		"Mana Steal",
		"Damage Modifier",
		"Bonus Health",
		"Bonus Mana",
		"Bonus Stam",
		"Bonus Health Regen",
		"Bonus Mana Regen",
		"Bonus Stam Regen",
		"Mana",
		"Stam"
	};

	public static NBTTagCompound writeStatData(NBTTagCompound tag, Stat[] stats, String prefix)
	{
		for (int index = 0; index < stats.length; index++)
		{
			tag.setFloat(prefix + stats[index].getNBTTag(), stats[index].getValue());
		}
		return tag;
	}

	public static Stat[] readStatData(NBTTagCompound tag, String prefix)
	{
		Stat[] stats = returnEmptyStatMatrix();
		for (int index = 0; index < stats.length; index++)
		{
			stats[index].setValue(tag.getFloat(prefix + stats[index].getNBTTag()));
		}
		return stats;
	}

	public static Stat[] returnEmptyStatMatrix()
	{
		Stat[] emptyStats = new Stat[STAT_NAMES.length];
		for (int i = 0; i < emptyStats.length; i++)
		{
			emptyStats[i] = new Stat(STAT_NAMES[i], STAT_TAGS[i], i).setValue(0f);
		}
		return emptyStats;
	}

	public static Stat[] setStatValues(Stat[] currentStats, float[] values, int startingIndex)
	{
		if (values != null)
		{
			for (int i = 0; i < values.length; i++)
			{
				currentStats[startingIndex + i].setValue(values[i]);
			}
		}
		return currentStats;
	}

	public static Stat[] setStatValues(Stat[] currentStats, float[] values)
	{
		return setStatValues(currentStats, values, 0);
	}

	public static Stat[] setStatValues(float[] values)
	{
		return setStatValues(returnEmptyStatMatrix(), values, 0);
	}
	
	public static Stat[] copyStats(Stat[] parentStats,Stat[] targetStats)
	{
		for(int i = 0; i < parentStats.length;i++)
		{
			targetStats[i].setValue(parentStats[i].getValue());
		}
		return targetStats;
	}

	public static String getNBTTag(int statId)
	{
		return STAT_TAGS[statId];
	}

	public static String getName(int statId)
	{
		return STAT_NAMES[statId];
	}

	public static boolean isStatPercent(int statId)
	{
		if (statId == ManagerRPGStat.LIFE_STEAL || statId == ManagerRPGStat.MANA_STEAL
				|| statId == ManagerRPGStat.ENHANCED_DAMAGE)
			return true;
		return false;
	}
}
