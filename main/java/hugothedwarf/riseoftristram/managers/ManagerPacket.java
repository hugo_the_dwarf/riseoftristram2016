package hugothedwarf.riseoftristram.managers;

import hugothedwarf.riseoftristram.RiseOfTristram;
import hugothedwarf.riseoftristram.comms.packets.ClassGUIPacket;
import hugothedwarf.riseoftristram.comms.packets.ClassGUIPacket.ClassGUIPacketHandler;
import hugothedwarf.riseoftristram.comms.packets.capabilities.CapAttributeResponsePacket;
import hugothedwarf.riseoftristram.comms.packets.capabilities.CapAttributeResponsePacket.CapAttributePacketHandler;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;

public class ManagerPacket
{

	public static SimpleNetworkWrapper INSTANCE = NetworkRegistry.INSTANCE
			.newSimpleChannel(RiseOfTristram.MODID + "htdchannel");

	public static void registerPackets()
	{
		int packetId = 0;
		INSTANCE.registerMessage(CapAttributePacketHandler.class, CapAttributeResponsePacket.class, packetId++,
				Side.CLIENT);

		INSTANCE.registerMessage(ClassGUIPacketHandler.class, ClassGUIPacket.class, packetId++, Side.SERVER);

	}

}
