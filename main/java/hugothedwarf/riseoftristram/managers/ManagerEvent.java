package hugothedwarf.riseoftristram.managers;

import hugothedwarf.riseoftristram.events.EventAddCapabilities;
import hugothedwarf.riseoftristram.events.EventBlockLogic;
import hugothedwarf.riseoftristram.events.EventEntityLogic;
import hugothedwarf.riseoftristram.events.EventItemLogic;
import hugothedwarf.riseoftristram.events.FMLEventPlayerTick;
import net.minecraftforge.common.MinecraftForge;

public class ManagerEvent
{

	public ManagerEvent()
	{

	}

	public static void registerEvents()
	{
		MinecraftForge.EVENT_BUS.register(new FMLEventPlayerTick());
		MinecraftForge.EVENT_BUS.register(new EventAddCapabilities());
		MinecraftForge.EVENT_BUS.register(new EventEntityLogic());
		MinecraftForge.EVENT_BUS.register(new EventItemLogic());
		MinecraftForge.EVENT_BUS.register(new EventBlockLogic());		
	}

}
