package hugothedwarf.riseoftristram.managers;

import hugothedwarf.riseoftristram.libs.skills.ISkill;
import hugothedwarf.riseoftristram.libs.skills.SkillDash;
import hugothedwarf.riseoftristram.libs.skills.SkillFireball;
import hugothedwarf.riseoftristram.libs.skills.SkillLeash;
import hugothedwarf.riseoftristram.libs.skills.SkillRepair;
import hugothedwarf.riseoftristram.libs.skills.SkillZombieCure;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;

public class ManagerSkill
{
	public static final String[] skillNames = new String[]
	{
		"Dash",
		"Cure Zombie",
		"Force Leash",
		"Fireball",
		"Repair"
	};
	public static final int SKILL_DASH = 0, SKILL_ZOMBIE_CURE = 1, SKILL_LEASH = 2, SKILL_FIREBALL = 3,
			SKILL_REPAIR = 4;
	
	public static int[] getAllSkillIDs()
	{
		return new int[] {SKILL_DASH,SKILL_ZOMBIE_CURE,SKILL_LEASH,SKILL_FIREBALL,SKILL_REPAIR};
	}

	public static ISkill getSkill(int skillId)
	{
		switch (skillId)
		{
			case SKILL_DASH:
				return new SkillDash();
			case SKILL_ZOMBIE_CURE:
				return new SkillZombieCure();
			case SKILL_LEASH:
				return new SkillLeash();
			case SKILL_FIREBALL:
				return new SkillFireball();
			case SKILL_REPAIR:
				return new SkillRepair();
			default:
				return null;
		}
	}

	public static ISkill[] loadSkills(int[] skillIDs)
	{
		ISkill[] outputSkills = new ISkill[skillNames.length];
		for(int i = 0; i < skillIDs.length;i++)
		{
			outputSkills[i] = getSkill(skillIDs[i]);
		}
		return outputSkills;
	}
}
