package hugothedwarf.riseoftristram.managers;

import java.util.ArrayList;
import java.util.Random;

import hugothedwarf.riseoftristram.libs.ModValues;
import hugothedwarf.riseoftristram.libs.UtilNBTHelper;
import hugothedwarf.riseoftristram.libs.componets.ItemAffix;
import hugothedwarf.riseoftristram.libs.componets.ItemAffixEnchantment;
import hugothedwarf.riseoftristram.libs.componets.Stat;
import hugothedwarf.riseoftristram.libs.nbt.NBTBoolean;
import net.minecraft.init.Enchantments;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemBow;
import net.minecraft.item.ItemShield;
import net.minecraft.item.ItemSword;
import net.minecraft.item.ItemTool;

public class ManagerItemAffix
{
	public ManagerItemAffix()
	{

	}

	private static Random rand = new Random();

	public static ItemAffix getPrefix(Item item, int level)
	{
		ItemAffix affix = null;
		for (int i = 0; i < ModValues.RECURSIVE_LOOP_MAX; i++)
		{
			affix = getAffix(item, level, prefixes[rand.nextInt(prefixes.length)]);
			if (affix != null) break;
		}
		return affix;
	}

	public static ItemAffix getSuffix(Item item, int level)
	{
		ItemAffix affix = null;
		for (int i = 0; i < ModValues.RECURSIVE_LOOP_MAX; i++)
		{
			affix = getAffix(item, level, suffixes[rand.nextInt(suffixes.length)]);
			if (affix != null) break;
		}
		return affix;
	}

	public static ItemAffix getAffix(Item item, int level, ItemAffix[] affixes)
	{
		ArrayList<ItemAffix> lAffixes = new ArrayList<ItemAffix>();
		for (ItemAffix a : affixes)
		{
			if (item instanceof ItemArmor && a.isForArmor())
			{
				if (correctRank(level, a))
				{
					lAffixes.add(a);
				}
			}
			else if ((item instanceof ItemSword || item instanceof ItemTool) && a.isForMelee())
			{
				if (correctRank(level, a))
				{
					lAffixes.add(a);
				}
			}
			else if (item instanceof ItemBow && a.isForBow())
			{
				if (correctRank(level, a))
				{
					lAffixes.add(a);
				}
			}
			else if (item instanceof ItemShield && a.isForShield())
			{
				if (correctRank(level, a))
				{
					lAffixes.add(a);
				}
			}
			else if ((!(item instanceof ItemSword) && !(item instanceof ItemBow) && !(item instanceof ItemTool)
					&& !(item instanceof ItemArmor) && !(item instanceof ItemShield)) && a.isForTrinket())
			{
				if (correctRank(level, a))
				{
					lAffixes.add(a);
				}
			}
		}
		if (!lAffixes.isEmpty()) return lAffixes.get(rand.nextInt(lAffixes.size()));
		return null;
	}

	private static boolean correctRank(int rank, ItemAffix affix)
	{
		if (rank >= affix.rankLowRequirement
				&& (rank <= affix.rankHighRequirement || affix.rankHighRequirement == ModValues.LEVEL_MAX))
			return true;
		return false;
	}

	public static ItemAffix[] damagePrefixes = new ItemAffix[]
	{
		new ItemAffix("Jagged", 1, 20).setStat(new Stat(ManagerRPGStat.ENHANCED_DAMAGE).setValue(10, 20)).setMelee(),
		new ItemAffix("Deadly", 5, 35).setStat(new Stat(ManagerRPGStat.ENHANCED_DAMAGE).setValue(21, 30)).setMelee(),
		new ItemAffix("Vicious", 15, 50).setStat(new Stat(ManagerRPGStat.ENHANCED_DAMAGE).setValue(31, 40)).setMelee(),
		new ItemAffix("Brutal", 25, 65).setStat(new Stat(ManagerRPGStat.ENHANCED_DAMAGE).setValue(41, 50)).setMelee(),
		new ItemAffix("Massive", 30, ModValues.LEVEL_MAX)
				.setStat(new Stat(ManagerRPGStat.ENHANCED_DAMAGE).setValue(51, 65))
				.setEnchantment(new ItemAffixEnchantment(Enchantments.SHARPNESS, 1)).setMelee(),
		new ItemAffix("Savage", 45, ModValues.LEVEL_MAX)
				.setStat(new Stat(ManagerRPGStat.ENHANCED_DAMAGE).setValue(66, 80))
				.setEnchantment(new ItemAffixEnchantment(Enchantments.SHARPNESS, 2)).setMelee(),
		new ItemAffix("Merciless", 40, ModValues.LEVEL_MAX)
				.setStat(new Stat(ManagerRPGStat.ENHANCED_DAMAGE).setValue(81, 100))
				.setEnchantment(new ItemAffixEnchantment(Enchantments.SHARPNESS, 3)).setMelee(),
		new ItemAffix("Ferocious", 55, ModValues.LEVEL_MAX)
				.setStat(new Stat(ManagerRPGStat.ENHANCED_DAMAGE).setValue(101, 150))
				.setEnchantment(new ItemAffixEnchantment(Enchantments.SHARPNESS, 4)).setMelee(),
		new ItemAffix("Cruel", 75, ModValues.LEVEL_MAX)
				.setStat(new Stat(ManagerRPGStat.ENHANCED_DAMAGE).setValue(151, 200))
				.setEnchantment(new ItemAffixEnchantment(Enchantments.SHARPNESS, 5)).setMelee(),
	};

	public static ItemAffix[] defencePrefixes = new ItemAffix[]
	{
		new ItemAffix("Sturdy", 1, 20).setStat(new Stat(ManagerRPGStat.DEFENCE_RATING).setValue(10f, 30f))
				.setEnchantment(new ItemAffixEnchantment(Enchantments.UNBREAKING, 1)).setArmor().setShield(),
		new ItemAffix("Strong", 5, 35).setStat(new Stat(ManagerRPGStat.DEFENCE_RATING).setValue(31, 40))
				.setEnchantment(new ItemAffixEnchantment(Enchantments.UNBREAKING, 1)).setArmor().setShield(),
		new ItemAffix("Glorious", 15, 50).setStats(new Stat[]
		{
			new Stat(ManagerRPGStat.DEFENCE_RATING).setValue(41, 50),
			new Stat(ManagerRPGStat.BONUS_HP).setValue(100f, 200f)
		}).setEnchantment(new ItemAffixEnchantment(Enchantments.UNBREAKING, 2)).setArmor().setShield(), new ItemAffix("Blessed", 25, 65).setStats(new Stat[]
		{
			new Stat(ManagerRPGStat.DEFENCE_RATING).setValue(51, 65),
			new Stat(ManagerRPGStat.BONUS_HP).setValue(201f, 400f)
		}).setEnchantment(new ItemAffixEnchantment(Enchantments.UNBREAKING, 2)).setArmor().setShield(), new ItemAffix("Saintly", 30, 80).setStats(new Stat[]
		{
			new Stat(ManagerRPGStat.DEFENCE_RATING).setValue(51, 65),
			new Stat(ManagerRPGStat.BONUS_HP).setValue(401f, 600f)
		}).setStat(new Stat(ManagerRPGStat.DEFENCE_RATING).setValue(66, 80)).setEnchantment(new ItemAffixEnchantment(Enchantments.UNBREAKING, 3)).setArmor().setShield(), new ItemAffix("Holy", 45, ModValues.LEVEL_MAX).setStats(new Stat[]
		{
			new Stat(ManagerRPGStat.DEFENCE_RATING).setValue(81, 100),
			new Stat(ManagerRPGStat.BONUS_HP).setValue(601f, 800f)
		}).setEnchantment(new ItemAffixEnchantment(Enchantments.UNBREAKING, 4)).setArmor().setShield(), new ItemAffix("Godly", 55, ModValues.LEVEL_MAX).setStats(new Stat[]
		{
			new Stat(ManagerRPGStat.DEFENCE_RATING).setValue(101, 200),
			new Stat(ManagerRPGStat.BONUS_HP).setValue(801f, 1000f)
		}).setEnchantment(new ItemAffixEnchantment(Enchantments.UNBREAKING, 5)).setArmor().setShield(),
	};

	public static ItemAffix[][] prefixes = new ItemAffix[][]
	{
		damagePrefixes,
		defencePrefixes
	};

	public static ItemAffix[] regenSuffixes = new ItemAffix[]
	{
		new ItemAffix("Honor", "of", 1, 60).setStat(new Stat(ManagerRPGStat.BONUS_HP_REGEN).setValue(1f, 3f)).setArmor()
				.setShield().setTrinket(),
		new ItemAffix("Regeneration", "of", 30, 80).setStat(new Stat(ManagerRPGStat.BONUS_HP_REGEN).setValue(3f, 5f))
				.setArmor().setShield().setTrinket(),
		new ItemAffix("Regrowth", "of", 50, ModValues.LEVEL_MAX)
				.setStat(new Stat(ManagerRPGStat.BONUS_HP_REGEN).setValue(5f, 7f)).setArmor().setShield().setTrinket(),
		new ItemAffix("Revivification", "of", 70, ModValues.LEVEL_MAX)
				.setStat(new Stat(ManagerRPGStat.BONUS_HP_REGEN).setValue(7f, 10f)).setArmor().setShield().setTrinket(),

		new ItemAffix("Jogger", "of the", 1, 60).setStat(new Stat(ManagerRPGStat.BONUS_STAM_REGEN).setValue(1f, 3f))
				.setArmor().setShield().setTrinket(),
		new ItemAffix("Runner", "of the", 30, 80).setStat(new Stat(ManagerRPGStat.BONUS_STAM_REGEN).setValue(3f, 5f))
				.setArmor().setShield().setTrinket(),
		new ItemAffix("Sprinter", "of the", 50, ModValues.LEVEL_MAX)
				.setStat(new Stat(ManagerRPGStat.BONUS_STAM_REGEN).setValue(5f, 7f)).setArmor().setShield()
				.setTrinket(),
		new ItemAffix("Marathons", "of", 70, ModValues.LEVEL_MAX)
				.setStat(new Stat(ManagerRPGStat.BONUS_STAM_REGEN).setValue(7f, 10f)).setArmor().setShield()
				.setTrinket(),

		new ItemAffix("Insight", "of", 1, 60).setStat(new Stat(ManagerRPGStat.BONUS_MANA_REGEN).setValue(1f, 3f))
				.setArmor().setShield().setTrinket(),
		new ItemAffix("Mind", "of the", 30, 80).setStat(new Stat(ManagerRPGStat.BONUS_MANA_REGEN).setValue(3f, 5f))
				.setArmor().setShield().setTrinket(),
		new ItemAffix("Clarity", "of", 50, ModValues.LEVEL_MAX)
				.setStat(new Stat(ManagerRPGStat.BONUS_MANA_REGEN).setValue(5f, 7f)).setArmor().setShield()
				.setTrinket(),
		new ItemAffix("Meditation", "of", 70, ModValues.LEVEL_MAX)
				.setStat(new Stat(ManagerRPGStat.BONUS_MANA_REGEN).setValue(7f, 10f)).setArmor().setShield()
				.setTrinket(),

		new ItemAffix("Equilibrium", "of", 70, ModValues.LEVEL_MAX).setStats(new Stat[]
		{
			new Stat(ManagerRPGStat.BONUS_HP_REGEN).setValue(7f, 10f),
			new Stat(ManagerRPGStat.BONUS_STAM_REGEN).setValue(7f, 10f),
			new Stat(ManagerRPGStat.BONUS_MANA_REGEN).setValue(7f, 10f)
		}).setArmor().setShield().setTrinket(),
	};

	public static ItemAffix[] specialSuffixes = new ItemAffix[]
	{
		new ItemAffix("Zodiac", "of the", 1, ModValues.LEVEL_MAX).setStats(new Stat[]
		{
			new Stat(ManagerRPGStat.STRENGTH).setValue(20f, 30f),
			new Stat(ManagerRPGStat.DEXTERITY).setValue(20f, 30f),
			new Stat(ManagerRPGStat.INTELLIGENCE).setValue(20f, 30f),
			new Stat(ManagerRPGStat.VITALITY).setValue(20f, 30f)
		}).setArmor().setMelee().setBow().setShield(), new ItemAffix("Restoration", "of", 1, ModValues.LEVEL_MAX).setEnchantment(new ItemAffixEnchantment(Enchantments.MENDING, 1)).setArmor().setMelee().setBow().setShield(), new ItemAffix("Ages", "of the", 1, ModValues.LEVEL_MAX).setNBT(new NBTBoolean(UtilNBTHelper.UNBREAKABLE, true)).setArmor().setMelee().setBow().setShield(),
	};

	public static ItemAffix[][] suffixes = new ItemAffix[][]
	{
		regenSuffixes,
		specialSuffixes
	};

}
