package hugothedwarf.riseoftristram.managers;

import java.util.ArrayList;
import java.util.Random;

import hugothedwarf.riseoftristram.libs.HTDUtil;
import hugothedwarf.riseoftristram.libs.ModValues;
import hugothedwarf.riseoftristram.libs.componets.Loot;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.ItemStack;

public class ManagerLoot
{
	private static int[] gold = new int[]
	{
		1,
		9,
		81,
		729,
		6561,
		59049,
		531441
	};
	private static ItemStack[] goldItems = new ItemStack[]
	{
		new ItemStack(Items.GOLD_NUGGET),
		new ItemStack(Items.GOLD_INGOT),
		new ItemStack(Blocks.GOLD_BLOCK),
		new ItemStack(Items.EMERALD),
		new ItemStack(Blocks.EMERALD_BLOCK),
		new ItemStack(Items.DIAMOND),
		new ItemStack(Blocks.DIAMOND_BLOCK)
	};

	public ManagerLoot()
	{

	}

	static Loot[] materialLoot = new Loot[]
	{
		new Loot(Items.COAL, 1, ModValues.LEVEL_MAX, 1, 1, 3),
		// new Loot(Items.CLAY_BALL, 1, ModValues.LEVEL_MAX, 1, 1, 8),
		// new Loot(Blocks.SAND, 1, ModValues.LEVEL_MAX, 1, 1, 4),
		// new Loot(Blocks.GRAVEL, 1, ModValues.LEVEL_MAX, 1, 1, 4),
		// new Loot(Items.FLINT, 1, ModValues.LEVEL_MAX, 1, 1, 3),
		// new Loot(Items.STICK, 1, ModValues.LEVEL_MAX, 1, 2, 4),
		// new Loot(Blocks.PLANKS, 1, ModValues.LEVEL_MAX, 4, 1, 6).setRandomMeta(BlockPlanks.EnumType.values().length),
		new Loot(Items.IRON_INGOT, 1, ModValues.LEVEL_MAX, 6, 1, 3),
		// new Loot(Items.GOLD_NUGGET, 30, ModValues.LEVEL_MAX, 4, 1, 6),
		new Loot(Items.REDSTONE, 1, ModValues.LEVEL_MAX, 10, 1, 4),
		// new Loot(Items.SADDLE, 50, ModValues.LEVEL_MAX, 50),
		new Loot(Items.DYE, 1, ModValues.LEVEL_MAX, 15, 3, 9).setMeta(EnumDyeColor.BLUE.getDyeDamage()),
		new Loot(Items.GLOWSTONE_DUST, 1, ModValues.LEVEL_MAX, 20, 1, 4),

			// new Loot(Items.EMERALD, 1, ModValues.LEVEL_MAX, 10, 1, 2),
			// new Loot(Items.DIAMOND, 50, ModValues.LEVEL_MAX, 12),

			// new Loot(Items.DRAGON_BREATH, 40, ModValues.LEVEL_MAX, 10),
	};

	static Loot[] toolLoot = new Loot[]
	{
		new Loot(Items.WOODEN_AXE, 1, 20, 1),
		new Loot(Items.WOODEN_PICKAXE, 1, 20, 1),
		new Loot(Items.WOODEN_SHOVEL, 1, 20, 1),
		new Loot(Items.STONE_AXE, 10, 40, 10),
		new Loot(Items.STONE_PICKAXE, 10, 40, 10),
		new Loot(Items.STONE_SHOVEL, 10, 40, 10),
		new Loot(Items.IRON_AXE, 20, 60, 20),
		new Loot(Items.IRON_PICKAXE, 20, 60, 20),
		new Loot(Items.IRON_SHOVEL, 20, 60, 20),
		new Loot(Items.GOLDEN_AXE, 20, 70, 20),
		new Loot(Items.GOLDEN_PICKAXE, 20, 70, 20),
		new Loot(Items.GOLDEN_SHOVEL, 20, 70, 20),
		new Loot(Items.DIAMOND_AXE, 60, ModValues.LEVEL_MAX, 60),
		new Loot(Items.DIAMOND_PICKAXE, 60, ModValues.LEVEL_MAX, 60),
		new Loot(Items.DIAMOND_SHOVEL, 60, ModValues.LEVEL_MAX, 60),
	};

	static Loot[] weaponLoot = new Loot[]
	{
		new Loot(Items.BOW, 20, ModValues.LEVEL_MAX, 20),
		new Loot(Items.WOODEN_SWORD, 1, 20, 1),
		new Loot(Items.STONE_SWORD, 10, 40, 10),
		new Loot(Items.IRON_SWORD, 20, 60, 20),
		new Loot(Items.GOLDEN_SWORD, 20, 70, 20),
		new Loot(Items.DIAMOND_SWORD, 60, ModValues.LEVEL_MAX, 60),
	};

	static Loot[] armorLoot = new Loot[]
	{
		new Loot(Items.SHIELD, 20, ModValues.LEVEL_MAX, 20),
		new Loot(Items.LEATHER_HELMET, 1, 20, 1),
		new Loot(Items.LEATHER_CHESTPLATE, 1, 20, 1),
		new Loot(Items.LEATHER_LEGGINGS, 1, 20, 1),
		new Loot(Items.LEATHER_BOOTS, 1, 20, 1),
		new Loot(Items.CHAINMAIL_HELMET, 10, 40, 10),
		new Loot(Items.CHAINMAIL_CHESTPLATE, 10, 40, 10),
		new Loot(Items.CHAINMAIL_LEGGINGS, 10, 40, 10),
		new Loot(Items.CHAINMAIL_BOOTS, 10, 40, 10),
		new Loot(Items.IRON_HELMET, 20, 60, 20),
		new Loot(Items.IRON_CHESTPLATE, 20, 60, 20),
		new Loot(Items.IRON_LEGGINGS, 20, 60, 20),
		new Loot(Items.IRON_BOOTS, 20, 60, 20),
		new Loot(Items.IRON_HORSE_ARMOR, 20, 60, 20),
		new Loot(Items.GOLDEN_HELMET, 20, 70, 20),
		new Loot(Items.GOLDEN_CHESTPLATE, 20, 70, 20),
		new Loot(Items.GOLDEN_LEGGINGS, 20, 70, 20),
		new Loot(Items.GOLDEN_BOOTS, 20, 70, 20),
		new Loot(Items.GOLDEN_HORSE_ARMOR, 20, 70, 20),
		new Loot(Items.DIAMOND_HELMET, 60, ModValues.LEVEL_MAX, 60),
		new Loot(Items.DIAMOND_CHESTPLATE, 60, ModValues.LEVEL_MAX, 60),
		new Loot(Items.DIAMOND_LEGGINGS, 60, ModValues.LEVEL_MAX, 60),
		new Loot(Items.DIAMOND_BOOTS, 60, ModValues.LEVEL_MAX, 60),
		new Loot(Items.DIAMOND_HORSE_ARMOR, 60, ModValues.LEVEL_MAX, 60),
	};

	private static Loot getLootFromList(Loot[] lootList, int points, int level)
	{
		Random random = new Random();
		ArrayList<Loot> newLoot = new ArrayList<Loot>();
		for (Loot l : lootList)
		{
			if (l.canLoot(points, level)) newLoot.add(l);
		}
		if (!newLoot.isEmpty()) return newLoot.get(random.nextInt(newLoot.size()));
		return null;
	}

	public static EntityItem[] addLoot(Entity entity)
	{
		Random random = new Random();
		HTDUtil htdU = new HTDUtil();

		int level = 5;
		int points = 5;
		int numOfItems = HTDUtil.advRandom(0.5f, 0.085f);
		numOfItems += 1;
		ArrayList<EntityItem> newList = new ArrayList<EntityItem>();
		ItemStack item = null;
		Loot loot = null;

		int goldDropped = level + points;
		int[] numberOfGoldItems = new int[]
		{
			0,
			0,
			0,
			0,
			0,
			0,
			0
		};
		while (goldDropped > 0)
		{
			for (int i = gold.length - 1; i >= 0; i--)
			{
				if (goldDropped >= gold[i])
				{
					goldDropped -= gold[i];
					numberOfGoldItems[i]++;
				}
			}
		}

		for (int i = 0; i < numberOfGoldItems.length; i++)
		{
			if (numberOfGoldItems[i] > 0)
			{
				while (numberOfGoldItems[i] > goldItems[i].getMaxStackSize())
				{
					ItemStack inputItem = goldItems[i];
					inputItem.stackSize = inputItem.getMaxStackSize();
					newList.add(new EntityItem(entity.worldObj, entity.getPosition().getX(),
							entity.getPosition().getY(), entity.getPosition().getZ(), inputItem));
					numberOfGoldItems[i] -= goldItems[i].getMaxStackSize();
				}
				if (numberOfGoldItems[i] > 0)
				{
					ItemStack inputItem = goldItems[i];
					inputItem.stackSize = numberOfGoldItems[i];
					newList.add(new EntityItem(entity.worldObj, entity.getPosition().getX(),
							entity.getPosition().getY(), entity.getPosition().getZ(), inputItem));
					numberOfGoldItems[i] = 0;
				}
			}
		}

		for (int i = 0; numOfItems > 0 || points > 0; i++)
		{
			if (numOfItems > 0)
			{
				numOfItems--;
				int itemType = random.nextInt(3);
				switch (itemType)
				{
					case 0:
						loot = getLootFromList(weaponLoot, points, level);
						break;
					case 1:
						loot = getLootFromList(toolLoot, points, level);
						break;
					case 2:
						loot = getLootFromList(armorLoot, points, level);
						break;
				}
			}
			else loot = getLootFromList(materialLoot, points, level);

			if (loot != null)
			{
				points -= loot.getCost();
				item = loot.get();
				if (!HTDUtil.isItemEquipment(item.getItem()) && !item.getItem().equals(Items.SADDLE))
					while (points >= loot.getCost() && random.nextInt(20) > 10)
					{
					points -= loot.getCost() > 1 ? loot.getCost() / 2 : 1;
					item.stackSize += loot.get().stackSize;
					}

				newList.add(new EntityItem(entity.worldObj, entity.getPosition().getX(), entity.getPosition().getY(),
						entity.getPosition().getZ(), item));
			}
			else break;
		}

		if (!newList.isEmpty())
		{
			EntityItem[] returnList = new EntityItem[newList.size()];
			for (int i = 0; i < returnList.length; i++)
			{
				returnList[i] = newList.get(i);
			}
			return returnList;
		}
		return null;
	}
}
