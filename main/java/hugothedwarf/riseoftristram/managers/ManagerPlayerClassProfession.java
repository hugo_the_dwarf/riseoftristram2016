package hugothedwarf.riseoftristram.managers;

import hugothedwarf.riseoftristram.libs.PlayerClass;
import hugothedwarf.riseoftristram.libs.PlayerProfession;

public class ManagerPlayerClassProfession
{
	// String className,int strStat,int dexStat,
	// int intStat,int vitStat,int agiStat,
	// float baseHp,float baseMana,float baseStam,
	// float hpPerVit,float manaPerIntStat,float stamPerAgiStat

	public static final PlayerClass defaultClass = new PlayerClass("Nomad", null,null);
	
	private static PlayerClass ranger = new PlayerClass("Ranger", new int[] {ManagerSkill.SKILL_DASH},ManagerRPGStat.setStatValues(new float[] {10}))
			.setDmgPerDex(1.325f).setDmgPerStr(0.15f).setHPPerVit(2.755f).setStamPerVitStat(6.875f);
	
	private static PlayerClass rogue = new PlayerClass("Rogue", new int[] {ManagerSkill.SKILL_DASH},ManagerRPGStat.setStatValues(new float[] {10}))
			.setDmgPerDex(1.325f).setHPPerVit(3.755f).setStamPerVitStat(5.875f);
	
	private static PlayerClass arcArcher = new PlayerClass("Arcane Archer", ManagerSkill.getAllSkillIDs(),ManagerRPGStat.setStatValues(new float[] {10}))
			.setDmgPerDex(1.75f).setDmgPerInt(1.75f).setHPPerVit(1.755f).setManaPerIntStat(2.45f).setStamPerVitStat(4.875f);
	
	private static PlayerClass barbarian = new PlayerClass("Barbarian", new int[] {ManagerSkill.SKILL_DASH},ManagerRPGStat.setStatValues(new float[] {10}))
			.setDmgPerStr(1.45f).setMaxDmgPercent(0.3575825f).setHPPerVit(3.25f).setStamPerVitStat(8.35f);
			
	public static PlayerClass[] classes = new PlayerClass[]
	{
		ranger,
		rogue,
		arcArcher,
		barbarian,
//		new PlayerClass("Spellsword", null).setDmgPerStr(1.15f).setDmgPerInt(1.75f).setMaxDmgPercent(0.265f).setHPPerVit(2.25f).setManaPerIntStat(2.25f).setStamPerVitStat(6.35f),
//		new PlayerClass("Battle Cleric", new int[]
//		{
//			ManagerSkill.SKILL_ZOMBIE_CURE
//		}).setDmgPerStr(1f).setDmgPerInt(2.15f).setHPPerVit(3f).setManaPerIntStat(3f).setStamPerVitStat(3f),
//		new PlayerClass("Elementalist", new int[]
//		{
//			ManagerSkill.SKILL_FIREBALL
//		}).setDmgPerInt(1.45f).setMaxDmgPercent(0.3575825f).setManaPerIntStat(3f),
//		new PlayerClass("Druid", null),
//		new PlayerClass("Healer", new int[]
//		{
//			ManagerSkill.SKILL_ZOMBIE_CURE
//		}),
//		new PlayerClass("Knight", null),
//		new PlayerClass("Paladin", null)
	};

	public static String professionMiner = "Miner";
	public static String professionFarmer = "Farmer";
	public static String professionBlacksmith = "Blacksmith";
	public static String professionScholar = "Arcane Scholar";

	public static PlayerProfession[] professions = new PlayerProfession[]
	{
		new PlayerProfession(professionMiner, "Digging stone blocks is faster, ore yields more, and a rare chance to get lesser materials from mining.", null),
		new PlayerProfession(professionFarmer, "Harvesting plants has a chance for a greater yield, can cut wood faster, does more damage to livestock.", new int[]
		{
			ManagerSkill.SKILL_LEASH
		}), new PlayerProfession(professionBlacksmith, "Can repair items, crafted gear has a better rank and quality.", new int[]
		{
			ManagerSkill.SKILL_REPAIR
		}), new PlayerProfession(professionScholar, "guy that does stuff", new int[]
		{
			ManagerSkill.SKILL_DASH,
			ManagerSkill.SKILL_ZOMBIE_CURE,
			ManagerSkill.SKILL_LEASH,
			ManagerSkill.SKILL_FIREBALL,
			ManagerSkill.SKILL_REPAIR
		})
	};
}
