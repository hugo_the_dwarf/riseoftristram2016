package hugothedwarf.riseoftristram.client.gui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

public class GuiHandler implements IGuiHandler
{

	public static final int CLASS_GUI = 0;

	@Override
	public Object getClientGuiElement(int guiId, EntityPlayer player, World world, int x, int y, int z)
	{
		// TileEntity entity = world.getTileEntity(new BlockPos(x, y, z));
		switch (guiId)
		{
			case CLASS_GUI:
				return new GuiPlayerStats(player);
			default:
				return null;
		}
	}

	@Override
	public Object getServerGuiElement(int guiId, EntityPlayer player, World world, int x, int y, int z)
	{
		// TileEntity entity = world.getTileEntity(new BlockPos(x, y, z));
		switch (guiId)
		{
			case CLASS_GUI:
				return new ContainerNull();
			default:
				return null;
		}
	}

}
