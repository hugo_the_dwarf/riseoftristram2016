package hugothedwarf.riseoftristram.client.gui;

import net.minecraft.entity.passive.EntityVillager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;

public class ContainerShop extends Container
{
	
	private EntityVillager villager;
	
	public ContainerShop(InventoryPlayer invPlayer, EntityVillager villager)
	{
		this.villager = villager;
	}

	@Override
	public boolean canInteractWith(EntityPlayer playerIn)
	{
		return !this.villager.isTrading();
	}

}
