package hugothedwarf.riseoftristram.client.gui;

import java.awt.Color;

import org.lwjgl.opengl.GL11;

import hugothedwarf.riseoftristram.RiseOfTristram;
import hugothedwarf.riseoftristram.capability.playerextra.CapPlayerExtra;
import hugothedwarf.riseoftristram.capability.playerextra.CapPlayerExtraData;
import hugothedwarf.riseoftristram.capability.rpgValues.CapRPG;
import hugothedwarf.riseoftristram.capability.rpgValues.CapRPGData;
import hugothedwarf.riseoftristram.comms.packets.ClassUpdatePacket;
import hugothedwarf.riseoftristram.comms.packets.ProfessionUpdatePacket;
import hugothedwarf.riseoftristram.comms.packets.SkillUpdatePacket;
import hugothedwarf.riseoftristram.events.client.KeyHandleEvent;
import hugothedwarf.riseoftristram.libs.ModValues;
import hugothedwarf.riseoftristram.libs.PlayerClass;
import hugothedwarf.riseoftristram.managers.ManagerRPGStat;
import hugothedwarf.riseoftristram.managers.ManagerPacket;
import hugothedwarf.riseoftristram.managers.ManagerPlayerClassProfession;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;

public class GuiPlayerStats extends GuiContainer
{
	public static final ResourceLocation texture = new ResourceLocation(RiseOfTristram.MODID.toLowerCase(),
			"textures/gui/base_gui.png");

	public static final ResourceLocation skill = new ResourceLocation(RiseOfTristram.MODID.toLowerCase(),
			"textures/gui/skills.png");

	private EntityPlayer player;
	private int pad = 4;
	private int ch = 20;
	private int selectedClass = -1;
	private int selectedProfession = -1;
	private int selectedSkill1 = -1;
	private int selectedSkill2 = -1;
	private int selectedSkill3 = -1;
	private int selectedSkill4 = -1;

	private CapPlayerExtraData capPlayerExtra;
	private CapRPGData capAttributes;
	private PlayerClass classPlayer;

	private int skillHW = 22;

	private final int CLASS_FORWARD = 0, CLASS_BACK = 1, CLASS_SUBMIT = 2, PROFESSION_FORWARD = 3, PROFESSION_BACK = 4,
			PROFESSION_SUBMIT = 5;
	private final int SKILL_SUBMIT = 6, SKILL_1_FORWARD = 7, SKILL_1_BACK = 8, SKILL_2_FORWARD = 9, SKILL_2_BACK = 10,
			SKILL_3_FORWARD = 11, SKILL_3_BACK = 12, SKILL_4_FORWARD = 13, SKILL_4_BACK = 14;

	public GuiPlayerStats(EntityPlayer player)
	{
		super(new ContainerNull());

		this.player = player;
		this.xSize = 176;
		this.ySize = 227;
		capPlayerExtra = player.getCapability(CapPlayerExtra.CAPABILITY, null);
		capAttributes = player.getCapability(CapRPG.CAPABILITY, null);
		classPlayer = capPlayerExtra.getCurrentClass();

		this.selectedClass = capPlayerExtra.getCurrentClassId();
		this.selectedProfession = capPlayerExtra.getCurrentProfessionId();
		if (capPlayerExtra.skillIds != null)
		{
			for (int index = 0; index < capPlayerExtra.skillIds.length; index++)
			{
				if (capPlayerExtra.skillIds[index] == capPlayerExtra.playerInfo[capPlayerExtra.SKILL_1])
					selectedSkill1 = index;
				if (capPlayerExtra.skillIds[index] == capPlayerExtra.playerInfo[capPlayerExtra.SKILL_2])
					selectedSkill2 = index;
				if (capPlayerExtra.skillIds[index] == capPlayerExtra.playerInfo[capPlayerExtra.SKILL_3])
					selectedSkill3 = index;
				if (capPlayerExtra.skillIds[index] == capPlayerExtra.playerInfo[capPlayerExtra.SKILL_4])
					selectedSkill4 = index;
			}
		}
	}

	/** Button Clicks **/
	@Override
	protected void actionPerformed(GuiButton button)
	{
		switch (button.id)
		{
			case CLASS_FORWARD: // Class Forward
				this.selectedClass++;
				this.selectedClass = this.selectedClass == ManagerPlayerClassProfession.classes.length ? 0
						: this.selectedClass;
				break;
			case CLASS_BACK: // Class Back
				this.selectedClass--;
				this.selectedClass = this.selectedClass < 0 ? ManagerPlayerClassProfession.classes.length - 1
						: this.selectedClass;
				break;
			case CLASS_SUBMIT: // Change Class
				ManagerPacket.INSTANCE.sendToServer(new ClassUpdatePacket(selectedClass));
				break;
			case PROFESSION_FORWARD: // Profession Forward
				this.selectedProfession++;
				this.selectedProfession = this.selectedProfession == ManagerPlayerClassProfession.professions.length ? 0
						: this.selectedProfession;
				break;
			case PROFESSION_BACK: // Profession Back
				this.selectedProfession--;
				this.selectedProfession = this.selectedProfession < 0
						? ManagerPlayerClassProfession.professions.length - 1 : this.selectedProfession;
				break;
			case PROFESSION_SUBMIT: // Change Profession
				ManagerPacket.INSTANCE.sendToServer(new ProfessionUpdatePacket(selectedProfession));
				break;
			case SKILL_1_FORWARD: // Class Forward
				this.selectedSkill1++;
				if (capPlayerExtra.skillIds != null)
					this.selectedSkill1 = this.selectedSkill1 == capPlayerExtra.skillIds.length ? 0
							: this.selectedSkill1;
				else this.selectedSkill1 = -1;
				break;
			case SKILL_1_BACK: // Class Back
				this.selectedSkill1--;
				if (capPlayerExtra.skillIds != null) this.selectedSkill1 = this.selectedSkill1 < 0
						? capPlayerExtra.skillIds.length - 1 : this.selectedSkill1;
				else this.selectedSkill1 = -1;
				break;
			case SKILL_2_FORWARD: // Class Forward
				this.selectedSkill2++;
				if (capPlayerExtra.skillIds != null)
					this.selectedSkill2 = this.selectedSkill2 == capPlayerExtra.skillIds.length ? 0
							: this.selectedSkill2;
				else this.selectedSkill2 = -1;
				break;
			case SKILL_2_BACK: // Class Back
				this.selectedSkill2--;
				if (capPlayerExtra.skillIds != null) this.selectedSkill2 = this.selectedSkill2 < 0
						? capPlayerExtra.skillIds.length - 1 : this.selectedSkill2;
				else this.selectedSkill2 = -1;
				break;

			case SKILL_3_FORWARD: // Class Forward
				this.selectedSkill3++;
				if (capPlayerExtra.skillIds != null)
					this.selectedSkill3 = this.selectedSkill3 == capPlayerExtra.skillIds.length ? 0
							: this.selectedSkill3;
				else this.selectedSkill3 = -1;
				break;
			case SKILL_3_BACK: // Class Back
				this.selectedSkill3--;
				if (capPlayerExtra.skillIds != null) this.selectedSkill3 = this.selectedSkill3 < 0
						? capPlayerExtra.skillIds.length - 1 : this.selectedSkill3;
				else this.selectedSkill3 = -1;
				break;
			case SKILL_4_FORWARD: // Class Forward
				this.selectedSkill4++;
				if (capPlayerExtra.skillIds != null)
					this.selectedSkill4 = this.selectedSkill4 == capPlayerExtra.skillIds.length ? 0
							: this.selectedSkill4;
				else this.selectedSkill4 = -1;
				break;
			case SKILL_4_BACK: // Class Back
				this.selectedSkill4--;
				if (capPlayerExtra.skillIds != null) this.selectedSkill4 = this.selectedSkill4 < 0
						? capPlayerExtra.skillIds.length - 1 : this.selectedSkill4;
				else this.selectedSkill4 = -1;
				break;

			case SKILL_SUBMIT: // Change Class
				if (selectedSkill1 != -1 || selectedSkill2 != -1 || selectedSkill3 != -1 || selectedSkill4 != -1)
					ManagerPacket.INSTANCE.sendToServer(
							new SkillUpdatePacket((selectedSkill1 != -1 ? capPlayerExtra.skillIds[selectedSkill1] : -1),
									(selectedSkill2 != -1 ? capPlayerExtra.skillIds[selectedSkill2] : -1),
									(selectedSkill3 != -1 ? capPlayerExtra.skillIds[selectedSkill3] : -1),
									(selectedSkill4 != -1 ? capPlayerExtra.skillIds[selectedSkill4] : -1)));
				break;
		}
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float f, int i, int j)
	{

		/* TextureManager manager = Minecraft.getMinecraft().renderEngine; manager.bindTexture(manager.getResourceLocation(1)); //RENDER ITEMS drawTexturedModelRectFromIcon(300, 300,
		 * RotItems.itemGunpowderInfuser.getIconFromDamage(0), 16, 16); */

		GL11.glColor4f(1F, 1F, 1F, 1F);
		Minecraft.getMinecraft().renderEngine.bindTexture(texture);
		drawTexturedModalRect(this.guiLeft, this.guiTop, 0, 0, this.xSize, this.ySize);

		this.buttonList.clear();
		// Start with main control buttons

		int x1 = this.guiLeft + this.pad;
		int x2 = (this.guiLeft + this.xSize) - (8 + this.pad);
		int x3 = ((x2 + x1) / 2) - 20;

		int y1 = this.guiTop + this.pad;

		int textY = this.guiTop + (8 * 4);
		int textYOffset = 10;
		int textX = x1 + pad;

		this.buttonList.add(new GuiGeneralButton(CLASS_BACK, x1, y1, "", 0));
		this.drawString(this.fontRendererObj,
				this.selectedClass != -1 ? ManagerPlayerClassProfession.classes[this.selectedClass].getName() : "None",
				x1 + 10, y1, 0xddeeee);
		this.buttonList.add(new GuiGeneralButton(CLASS_FORWARD, x1 + 90, y1, "", 1));
		this.buttonList.add(new GuiGeneralButton(CLASS_SUBMIT, x1 + 98, y1, "", 2));
		this.drawString(this.fontRendererObj, "<- Class", x1 + 116, y1, 0xddffdd);

		this.buttonList.add(new GuiGeneralButton(PROFESSION_BACK, x1, y1 + (8 * 2), "", 0));
		this.drawString(this.fontRendererObj,
				this.selectedProfession != -1
						? ManagerPlayerClassProfession.professions[this.selectedProfession].professionName : "None",
				x1 + 10, y1 + (8 * 2), 0xddeeee);
		this.buttonList.add(new GuiGeneralButton(PROFESSION_FORWARD, x1 + 90, y1 + (8 * 2), "", 1));
		this.buttonList.add(new GuiGeneralButton(PROFESSION_SUBMIT, x1 + 98, y1 + (8 * 2), "", 2));
		this.drawString(this.fontRendererObj, "<- Prof", x1 + 116, y1 + (8 * 2), 0xddffdd);

		this.drawString(this.fontRendererObj,
				"Current Class: " + (capPlayerExtra.getCurrentClassId() == -1 ? "None"
						: ManagerPlayerClassProfession.classes[capPlayerExtra.getCurrentClassId()].getName()),
				textX, textY, 0xddffdd);
		textY += textYOffset;

		this.drawString(this.fontRendererObj, "Current Prof : " + (capPlayerExtra.getCurrentProfessionId() == -1
				? "None"
				: ManagerPlayerClassProfession.professions[capPlayerExtra.getCurrentProfessionId()].professionName),
				textX, textY, 0xddffdd);
		textY += textYOffset;

		int textYDataTop = textY;
		for (int index = 0; index < capAttributes.baseStats.length; index++)
		{
			if (index != ManagerRPGStat.MIN_DAMAGE && index != ManagerRPGStat.MAX_DAMAGE && index != ManagerRPGStat.MANA
					&& index != ManagerRPGStat.STAM)
			{
				if (ManagerRPGStat.isStatPercent(index))
				{
					this.drawString(this.fontRendererObj,
							capAttributes.baseStats[index].getTag() + ": "
									+ String.format("%.2f", capAttributes.baseStats[index].getValuePercent()),
							textX, textY, capAttributes.baseStats[index].getValuePercent() == 0 ? 0xFFFFFF
									: capAttributes.baseStats[index].getValuePercent() > 0 ? 0x00ff42 : 0xff0c00);
					textY += textYOffset;
				}
				else
				{
					this.drawString(this.fontRendererObj,
							capAttributes.baseStats[index].getTag() + ": "
									+ String.format("%.0f", capAttributes.baseStats[index].getValue()),
							textX, textY, capAttributes.baseStats[index].getValue() == 0 ? 0xFFFFFF
									: capAttributes.baseStats[index].getValue() > 0 ? 0x00ff42 : 0xff0c00);
					textY += textYOffset;
				}
			}
		}

		int minDamage = capAttributes.baseStats[ManagerRPGStat.MIN_DAMAGE].getValueInt()
				+ (int) classPlayer.getMinDmg(capAttributes) + ModValues.BASE_MIN_DAMAGE;
		this.drawString(this.fontRendererObj,
				capAttributes.baseStats[ManagerRPGStat.MIN_DAMAGE].getTag() + ": " + minDamage, textX, textY,
				minDamage == 0 ? 0xFFFFFF : minDamage > 0 ? 0x00ff42 : 0xff0c00);
		textY += textYOffset;

		int maxDamage = capAttributes.baseStats[ManagerRPGStat.MAX_DAMAGE].getValueInt()
				+ (int) classPlayer.getMaxDmg(capAttributes) + ModValues.BASE_MAX_DAMAGE;

		this.drawString(this.fontRendererObj,
				capAttributes.baseStats[ManagerRPGStat.MAX_DAMAGE].getTag() + ": " + maxDamage, textX, textY,
				maxDamage == 0 ? 0xFFFFFF : maxDamage > 0 ? 0x00ff42 : 0xff0c00);
		textY += textYOffset;

		textY = textYDataTop;
		textX += 82;

		// for (int index = 0; index < capAttributes.stats.length; index++)
		// {
		// if (index != ManagerCapStat.MANA && index != ManagerCapStat.STAM)
		// {
		// this.drawString(this.fontRendererObj, capAttributes.stats[index].getTag() + ": " + String.format("%.2f", capAttributes.stats[index].getValue()), textX, textY,
		// capAttributes.stats[index].getValue() == 0 ? 0xFFFFFF : capAttributes.stats[index].getValue() > 0 ? 0x00ff42 : 0xff0c00);
		// textY += textYOffset;
		// }
		// }

		int skillX = skillHW;
		int skillY = 0;

		if (capPlayerExtra.skillIds != null)
		{
			skillX = (selectedSkill1 != -1 ? skillHW
					* (selectedSkill1 >= capPlayerExtra.skillIds.length ? 1 : capPlayerExtra.skillIds[selectedSkill1])
					: skillHW);
			skillY = (selectedSkill1 != -1 ? (selectedSkill1 >= capPlayerExtra.skillIds.length ? 0 : skillHW) : 0);
		}
		GL11.glColor4f(1F, 1F, 1F, 1F);
		Minecraft.getMinecraft().renderEngine.bindTexture(skill);

		drawTexturedModalRect(textX, textY, 0, 0, this.skillHW, this.skillHW);
		this.buttonList.add(new GuiGeneralButton(SKILL_1_BACK, textX + skillHW, textY, "", 0));
		this.buttonList.add(new GuiGeneralButton(SKILL_1_FORWARD, textX + skillHW + 8, textY, "", 1));
		drawTexturedModalRect(textX, textY, skillX, skillY, this.skillHW, this.skillHW);

		if (capPlayerExtra.skillIds != null)
		{
			skillX = (selectedSkill2 != -1 ? skillHW
					* (selectedSkill2 >= capPlayerExtra.skillIds.length ? 1 : capPlayerExtra.skillIds[selectedSkill2])
					: skillHW);
			skillY = (selectedSkill2 != -1 ? (selectedSkill2 >= capPlayerExtra.skillIds.length ? 0 : skillHW) : 0);
		}
		drawTexturedModalRect(textX, textY + skillHW, 0, 0, this.skillHW, this.skillHW);
		this.buttonList.add(new GuiGeneralButton(SKILL_2_BACK, textX + skillHW, textY + skillHW, "", 0));
		this.buttonList.add(new GuiGeneralButton(SKILL_2_FORWARD, textX + skillHW + 8, textY + skillHW, "", 1));
		drawTexturedModalRect(textX, textY + skillHW, skillX, skillY, this.skillHW, this.skillHW);

		if (capPlayerExtra.skillIds != null)
		{
			skillX = (selectedSkill3 != -1 ? skillHW
					* (selectedSkill3 >= capPlayerExtra.skillIds.length ? 1 : capPlayerExtra.skillIds[selectedSkill3])
					: skillHW);
			skillY = (selectedSkill3 != -1 ? (selectedSkill3 >= capPlayerExtra.skillIds.length ? 0 : skillHW) : 0);
		}
		GL11.glColor4f(1F, 1F, 1F, 1F);
		Minecraft.getMinecraft().renderEngine.bindTexture(skill);

		drawTexturedModalRect(textX, textY + (skillHW * 2), 0, 0, this.skillHW, this.skillHW);
		this.buttonList.add(new GuiGeneralButton(SKILL_3_BACK, textX + skillHW, textY + (skillHW * 2), "", 0));
		this.buttonList.add(new GuiGeneralButton(SKILL_3_FORWARD, textX + skillHW + 8, textY + (skillHW * 2), "", 1));
		drawTexturedModalRect(textX, textY + (skillHW * 2), skillX, skillY, this.skillHW, this.skillHW);

		if (capPlayerExtra.skillIds != null)
		{
			skillX = (selectedSkill4 != -1 ? skillHW
					* (selectedSkill4 >= capPlayerExtra.skillIds.length ? 1 : capPlayerExtra.skillIds[selectedSkill4])
					: skillHW);
			skillY = (selectedSkill4 != -1 ? (selectedSkill4 >= capPlayerExtra.skillIds.length ? 0 : skillHW) : 0);
		}
		drawTexturedModalRect(textX, textY + (skillHW * 3), 0, 0, this.skillHW, this.skillHW);
		this.buttonList.add(new GuiGeneralButton(SKILL_4_BACK, textX + skillHW, textY + (skillHW * 3), "", 0));
		this.buttonList.add(new GuiGeneralButton(SKILL_4_FORWARD, textX + skillHW + 8, textY + (skillHW * 3), "", 1));
		drawTexturedModalRect(textX, textY + (skillHW * 3), skillX, skillY, this.skillHW, this.skillHW);

		this.buttonList.add(new GuiGeneralButton(SKILL_SUBMIT, textX, textY + (skillHW * 4), "", 2));

		this.drawString(this.fontRendererObj, KeyHandleEvent.getKeyBindingName(KeyHandleEvent.skill1), textX + skillHW,
				textY + skillHW - (skillHW / 2), Color.WHITE.getRGB());
		this.drawString(this.fontRendererObj, KeyHandleEvent.getKeyBindingName(KeyHandleEvent.skill2), textX + skillHW,
				textY + (skillHW * 2) - (skillHW / 2), Color.WHITE.getRGB());
		this.drawString(this.fontRendererObj, "Sneak + " + KeyHandleEvent.getKeyBindingName(KeyHandleEvent.skill1),
				textX + skillHW, textY + (skillHW * 3) - (skillHW / 2), Color.WHITE.getRGB());
		this.drawString(this.fontRendererObj, "Sneak + " + KeyHandleEvent.getKeyBindingName(KeyHandleEvent.skill2),
				textX + skillHW, textY + (skillHW * 4) - (skillHW / 2), Color.WHITE.getRGB());
	}

	/** Keyboard Clicks **/
	@Override
	protected void keyTyped(char par1, int par2)
	{
		if ((par2 == 1) || (par2 == this.mc.gameSettings.keyBindInventory.getKeyCode()))
		{
			this.mc.thePlayer.closeScreen();
		}
	}
}
