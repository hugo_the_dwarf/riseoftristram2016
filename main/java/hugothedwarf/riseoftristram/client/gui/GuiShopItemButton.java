package hugothedwarf.riseoftristram.client.gui;

import java.awt.Color;

import hugothedwarf.riseoftristram.RiseOfTristram;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.SoundHandler;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class GuiShopItemButton extends GuiButton
{
	private ItemStack item = null;
	private boolean selected = false;
	private int type = 0; // 0 is buy, 1 is sell
	private int dim = 18;
	private static final ResourceLocation guiButtons = new ResourceLocation(RiseOfTristram.MODID + ":textures/gui/shop_item_box.png");

	public GuiShopItemButton(int buttonId, int x, int y, String buttonText, ItemStack item, int typeBuySell)
	{
		super(buttonId, x, y, buttonText);
		this.type = typeBuySell;
		this.item = item;
		this.width = dim;
		this.height = dim;
	}

	public int getType()
	{
		return this.type;
	}	
	
	@Override
	public void playPressSound(SoundHandler soundHandlerIn)
	{
		this.selected = !this.selected;
		super.playPressSound(soundHandlerIn);
	}

	@Override
	public void drawButton(Minecraft mc, int mouseX, int mouseY)
	{
		if (this.visible)
		{

			// TextureManager manager = Minecraft.getMinecraft().renderEngine;
			//
			// manager.bindTexture(manager.);
			// // RENDER ITEMS
			// drawTexturedModelRectFromIcon(300, 300, RotItems.itemGunpowderInfuser.getIconFromDamage(0), 16, 16);

			FontRenderer fontrenderer = mc.fontRendererObj;
			mc.getTextureManager().bindTexture(guiButtons);
			GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
			this.hovered = mouseX >= this.xPosition && mouseY >= this.yPosition && mouseX < this.xPosition + this.width && mouseY < this.yPosition + this.height;
			GlStateManager.enableBlend();
			GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
			GlStateManager.blendFunc(770, 771);

			this.drawTexturedModalRect(this.xPosition, this.yPosition, 0, 0, this.dim, this.dim);

			if (item != null)
			{
				RenderHelper.enableGUIStandardItemLighting();
				Minecraft.getMinecraft().getRenderItem().renderItemIntoGUI(item, this.xPosition + 1, this.yPosition + 1);
				RenderHelper.disableStandardItemLighting();
			}

			if (hovered || selected)
			{
				mc.getTextureManager().bindTexture(guiButtons);
				this.drawTexturedModalRect(this.xPosition, this.yPosition, 0, this.dim * (1 + type), this.dim, this.dim);
			}

			if (this.type == 1 && this.item.getMaxStackSize() > 1)
			{
				this.drawCenteredString(Minecraft.getMinecraft().fontRendererObj, "" + this.item.stackSize, this.xPosition + 10, this.yPosition + 4, Color.WHITE.getRGB());
			}

			this.mouseDragged(mc, mouseX, mouseY);
			int l = 14737632;

			if (packedFGColour != 0)
			{
				l = packedFGColour;
			}
			else if (!this.enabled)
			{
				l = 10526880;
			}
			else if (this.hovered)
			{
				l = 16777120;
			}

			this.drawCenteredString(fontrenderer, this.displayString, this.xPosition + this.width / 2, this.yPosition + (this.height - 8) / 2, l);
		}
	}

}
