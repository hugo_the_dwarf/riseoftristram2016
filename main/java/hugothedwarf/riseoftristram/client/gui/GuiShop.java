package hugothedwarf.riseoftristram.client.gui;

import java.util.ArrayList;

import org.lwjgl.opengl.GL11;

import hugothedwarf.riseoftristram.RiseOfTristram;
import hugothedwarf.riseoftristram.capability.playerextra.CapPlayerExtra;
import hugothedwarf.riseoftristram.capability.playerextra.CapPlayerExtraData;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class GuiShop extends GuiContainer
{
	private static final ResourceLocation texture = new ResourceLocation(RiseOfTristram.MODID.toLowerCase(),
			"textures/gui/shop_gui.png");

	private EntityPlayer player;
	private CapPlayerExtraData capPlayerExtra;

	private int frameSize = 18;

	private int itemStartX = 5;
	private int shopItemStartY = 6;
	private int playerHotbarStarY = 141;

	private ArrayList<ItemStack> itemsToBuy = new ArrayList<ItemStack>();
	private ArrayList<ItemStack> itemsToSell = new ArrayList<ItemStack>();

	private int buttonId = 0;
	private int buttonLastSellId = 0;
	private boolean hasAddedMercantItems = false;
	private boolean hasRequestedMerchantItems = false;

	public GuiShop(EntityPlayer player)
	{
		super(new ContainerNull());

		this.player = player;
		this.xSize = 256;
		this.ySize = 166;
		capPlayerExtra = player.getCapability(CapPlayerExtra.CAPABILITY, null);
	}

	private void initMerchantShopButtons()
	{
		int currentItemId = 0;
		int currentNormalItemId = 0;
		ItemStack[] items = new ItemStack[]
		{
			new ItemStack(Items.COAL),
			new ItemStack(Items.CLAY_BALL),
			new ItemStack(Blocks.SAND),
			new ItemStack(Blocks.GRAVEL),
			// new ItemStack(Items.FLINT),
			// new ItemStack(Items.STICK),
			// new ItemStack(Blocks.PLANKS),
			new ItemStack(Items.IRON_INGOT),
			new ItemStack(Items.REDSTONE),
			new ItemStack(Items.GLOWSTONE_DUST),
			new ItemStack(Items.DYE, 1, EnumDyeColor.BLUE.getDyeDamage()),
			new ItemStack(Items.DRAGON_BREATH),
			new ItemStack(Items.SADDLE),
			new ItemStack(Items.LEATHER),
		};

		for (int row = 0; row < 3; row++)
		{
			for (int column = 0; column < 9; column++)
			{
				if (currentItemId < items.length)
					buttonList.add(new GuiShopItemButton(buttonId++, guiLeft + itemStartX + (frameSize * column),
							guiTop + shopItemStartY + (frameSize * row), "", items[currentItemId++], 0));
				// else if (capMerchantData.normalItems != null && capMerchantData.normalItems.length > 0)
				// {
				// buttonList.add(new GuiShopItemButton(buttonId++, guiLeft + itemStartX + (frameSize * column),
				// guiTop + shopItemStartY + (frameSize * row), "",
				// capMerchantData.normalItems[currentNormalItemId++], 0));
				// }
			}
		}
	}

	@Override
	public void initGui()
	{
		super.initGui();

		// testing visual for now
		initMerchantShopButtons();

		int playerInventoryIndex = 0;

		for (int column = 0; column < 9; column++)
		{
			if (player.inventory.mainInventory[playerInventoryIndex] != null)
				buttonList.add(new GuiShopItemButton(buttonId++, guiLeft + itemStartX + (frameSize * column),
						guiTop + playerHotbarStarY, "", player.inventory.mainInventory[playerInventoryIndex], 1));
			playerInventoryIndex++;
		}

		for (int row = 0; row < 3; row++)
		{
			for (int column = 0; column < 9; column++)
			{
				if (player.inventory.mainInventory[playerInventoryIndex] != null)
					buttonList.add(new GuiShopItemButton(buttonId++, guiLeft + itemStartX + (frameSize * column),
							(guiTop + playerHotbarStarY - (2 + (frameSize * 3))) + (frameSize * row), "",
							player.inventory.mainInventory[playerInventoryIndex], 1));
				playerInventoryIndex++;
			}
		}
		buttonLastSellId = buttonId;
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY)
	{
		if (!hasAddedMercantItems)
		{
			// send request packet
			// ManagerPacket.INSTANCE.sendToServer(new CapMerchantNormalItemsPacket(merchant.getEntityId()));
			hasAddedMercantItems = true;
		}
		// if (capMerchantData.normalItems != null)
		// {
		// initMerchantShopButtons();
		// }

		GL11.glColor4f(1F, 1F, 1F, 1F);
		Minecraft.getMinecraft().renderEngine.bindTexture(texture);
		drawTexturedModalRect(this.guiLeft, this.guiTop, 0, 0, this.xSize, this.ySize);
	}

}
